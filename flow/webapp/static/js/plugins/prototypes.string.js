String.prototype.format=function(){var a=arguments;return this.replace(/\{(\d+)\}/g,function(m,n){return a[n];});};
String.prototype.jinjaFormat=function(){var a=arguments[0],z=arguments[1];return (a===undefined)?this:(a.constructor===Object)?(this.replace(/\{\{ *[a-zA-Z0-9_.]+ *\}\}/g,function(x){var k=x.replace(/\{|\}|\ /g, '').split('.'),b=a;while(k.length&&(b=b[k.shift()]));return (b===undefined && z!==undefined)?z:b;})):this;};
String.prototype.titleCase=function(){var s=this.toLowerCase().split(' ');for(var i=0;i<s.length; i++){s[i]='{0}{1}'.format(s[i].charAt(0).toUpperCase(),s[i].slice(1));}return s.join(' ');};
String.prototype.in=function(){return (arguments[0]===undefined)?false:(arguments[0].constructor===Array)?(arguments[0].indexOf(String(this))===-1)?false:true:false;};
String.prototype.normalize=function(){return this.replace(/\_/g, ' ')};
