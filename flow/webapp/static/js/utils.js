var $dom={window:$(window),document:$(document),html:$('html'),body:$('body')};

function isDefined(value){return typeof value != 'undefined'};
function getResponsiveBreakpoint(){let envs=['xs','sm', 'md','lg','xl'];let env='';let $e=$('<div>');$e.appendTo($dom.body);for(var i=envs.length-1;i>=0;i--){env=envs[i];$e.addClass('d{0}-none'.format((env=='xs')?'':'-'+env));if($e.is(':hidden')){break;}};$e.remove();return env;};

$.fn.hideCustom = function(){this.each(function(){let $el=$(this);$el.hide();$el.addClass('invisible')})};
$.fn.showCustom = function(){this.each(function(){let $el=$(this);$el.show();$el.removeClass('invisible')})};
$.fn.resetCustom = function(){this.each(function(){let $el=$(this);$el.removeAttr('data-modified');if(!isDefined($el.attr('readonly'))){ if(!isDefined($el.attr('data-reset-ignore'))){if($el.is('input,select,textarea')){if($el.attr('type')==='checkbox'){$el.prop('checked',false)}else{$el.val(null)};if(isDefined($el.attr('data-default-value'))){if($el.attr('type')=='checkbox'){$el.prop('checked',parseInt($el.attr('data-default-value')))}else{$el.val($el.attr('data-default-value'))}}$el.trigger('change').trigger('input');}else{$el.find('input,select,textarea').resetCustom()}}}})}

var ajax_dropdown_cache={};
var ajax_dropdown_queue=[];
$.fn.ajaxDropdown = function(){
    let $sel = $(this);
    let result = [];
    let labels = $sel.attr('data-ajax-select-label').split(',');
    let ajax_query = $sel.attr('data-ajax-select-target');

    if (isDefined(ajax_dropdown_cache[ajax_query])) {
        result = ajax_dropdown_cache[ajax_query];
        let options = {
            valueField:  $sel.attr('data-ajax-select-value'),
            searchField: ['text'],
            labelField: 'text',
            options: result,
            placeholder: $sel.attr('placeholder'),
            dropdownParent: $sel.closest('.form-group'),
            debug: true
        }
        $sel.selectize(options);
        if (isDefined($sel.attr('data-default-value'))) {
            $sel[0].selectize.setValue($sel.attr('data-default-value'));
        }
    } else {
        $.get($sel.attr('data-ajax-select-target'))
        .done(function(response) {
            let placeholder = {
                'text': $sel.attr('placeholder')
            };
            placeholder[$sel.attr('data-ajax-select-value')] = '';
            result.push(placeholder);
            $.each(response.data, function(i1, v) {
                result.push(v);
                result[i1+1]['text'] = '';
                $.each(labels, function(i2, att) {
                    result[i1+1]['text'] += (i2 > 0) ? ' - ' : '';
                    result[i1+1]['text'] += v[att];
                })
            });
            ajax_dropdown_cache[ajax_query] = result;
            let options = {
                valueField:  $sel.attr('data-ajax-select-value'),
                searchField: ['text'],
                labelField: 'text',
                options: result,
                placeholder: $sel.attr('placeholder'),
                dropdownParent: $sel.closest('.form-group'),
                debug: true
            }
            $sel.selectize(options);
            if (isDefined($sel.attr('data-default-value'))) {
                $sel[0].selectize.setValue($sel.attr('data-default-value'));
            }
        });
    }
}

$dom.document.on('click', 'button[data-reset-form]', function() {
    let $self = $(this);
    let $form = (isDefined($self.attr('data-reset-form'))) ? $($self.attr('data-reset-form')) : $self.closest('form');
    if ($form.length) {
        let form = $form[0];
        Swal.fire({
            type: 'question',
            title: 'Reset Form',
            text: 'All unsaved information will be cleared',
            showCancelButton: true,
            focusCancel: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $self.ladda().ladda('start');
                $(form).find('[data-form-dynamic][data-form-dynamic!="default"]').remove();
                $.each($(form).find('[data-ajax-select-target]'), function(i, v) {
                    if (isDefined($(v)[0].selectize)) {
                        $(v)[0].selectize.setValue('');
                    }
                })
                $(form).resetCustom();
                $self.ladda('stop');
            }
        });
    } else {
        console.debug('Invalid reset target');
    }
}).on('click', 'button[data-submit-form]', function(event) {
    event.preventDefault();
    event.stopPropagation();
    let $self = $(this);
    let $form = isDefined($self.attr('data-submit-form')) ? $($self.attr('data-submit-form')) : $self.closest('form');
    if ($form.length) {
        let form = $form[0];
        if (form.checkValidity()) {
            $self.ladda().ladda('start');
            $(form).trigger('submit');
        } else {
            $(form).find(':submit').click();
        }
    } else {
        console.debug('Invalid submit target');
    }
});

// Bootstrap
$dom.document.on('show.bs.modal', '.modal', function() {
    let zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function() {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});

// NowUI
var nowui = {navbar_visible: false};
merge_objects($dom, {body_click: $('<div id="bodyClick"></div>')});

$dom.document.on('show.bs.collapse', '.collapse', function() {
    let $el = $(this), $nav = $el.closest('.navbar'), default_class = $nav.attr('data-default-bg'), alt_class = $nav.attr('data-alt-bg');
    default_class = isDefined(default_class) ? default_class : 'navbar-transparent';
    alt_class = isDefined(alt_class) ? alt_class : 'bg-white';
    if (default_class != alt_class) {
        $nav.removeClass(default_class).addClass(alt_class);
    }
}).on('hide.bs.collapse', '.collapse', function() {
    let $el = $(this), $nav = $el.closest('.navbar'), default_class = $nav.attr('data-default-bg'), alt_class = $nav.attr('data-alt-bg');
    default_class = isDefined(default_class) ? default_class : 'navbar-transparent';
    alt_class = isDefined(alt_class) ? alt_class : 'bg-white';
    if (default_class != alt_class) {
        $nav.addClass(default_class).removeClass(alt_class);
    }
});

$dom.document.on('focus', '.form-control', function() {
    $(this).parent('.input-group').addClass('input-group-focus');
}).on('blur', '.form-control', function() {
    $(this).parent('.input-group').removeClass('input-group-focus');
});

$dom.document.on('click', '.navbar-toggler', function() {
    let $el = $(this);
    if (nowui.navbar_visible) {
        if (!$el.attr('data-target')) {
            $dom.html.removeClass('nav-open');
            nowui.navbar_visible = false;
        }
        $dom.body_click.detach();
        setTimeout(function() {
            $('.navbar-collapse{0}'.format($el.attr('data-target'))).collapse('hide');
            $el.removeClass('toggled');
        }, 350);
    } else {
        setTimeout(function() {
            $el.addClass('toggled');
        }, 350);
        $dom.body_click.appendTo($dom.body).off().on('click', function() {
            $dom.html.removeClass('nav-open');
            nowui.navbar_visible = false;
            setTimeout(function() {
                $('.navbar-collapse{0}'.format($el.attr('data-target'))).collapse('hide');
                $el.removeClass('toggled');
                $dom.body_click.detach();
            }, 350);
        });
        if (!$el.attr('data-target')) {
            $dom.html.addClass('nav-open');
            nowui.navbar_visible = true;
        }
    }
});

// $dom.document.on('change', 'select[data-ajax-select-target]', function(e) {
//     $(this).removeAttr('data-ajax-select-selected');
//     $form = $(this).closest('form');
//     if ($form.length) {
//         form = $form[0];
//         $(form).attr('data-modified', true);
//     }
// });

// $dom.document.on('keyup mouseup input', 'form .form-control', function() {
//     $(this).closest('form').attr('data-modified', true);
// });

// Plugin Inits
$dom.document.ready(function() {
    let isWindows = function() {return (navigator.platform.indexOf('Win')>-1)?true:false}();
    if (isWindows) {
        if ($('.sidebar .sidebar-wrapper, .main-panel').length) {
            new PerfectScrollbar('.sidebar .sidebar-wrapper, .main-panel');
        }
    }
    $.each($('[data-ajax-select-target]'), function(i, v) {
        $(v).ajaxDropdown();
    })
});
