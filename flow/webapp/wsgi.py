from common.pre_check import PreCheck
pre_check = PreCheck(required_python_version='3.6.5')
pre_check.enforce()

from common.utils.txt_reader import TxtReader
TxtReader('logo.txt', pre_check).render()

from gevent import monkey
monkey.patch_all()

import models
models.initialize()

from modules import admin, api
from common.flask.factory import app_factory

admin.create_app()
api.create_app()
app = app_factory.bundle()
