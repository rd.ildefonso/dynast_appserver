from flask import Blueprint, session, request, current_app as app
from common.utils.jsonify import jsonify
from common.utils.rest import parse_args
from common.flask.decorators import decorator_factory
from models.formula import Formula as ModuleModel

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix=f'/v1/{MODULE_NAME}')
decorator_factory.integrate_to_blueprint(bp)


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def get_one():
    filters, fields = parse_args(request.args)
    return jsonify(ModuleModel.find_one(filters, fields) or {'error': 'Record not found'})


@bp.route('/all', methods=['GET'])
@bp.csrf_protect
def get_all():
    filters, fields = parse_args(request.args)
    r_key = f'api{request.path}?{request.query_string.decode("utf-8")}'

    if ModuleModel.cache_exists(r_key):
        result = ModuleModel.cache_get(r_key)
    else:
        result = {'data': list(ModuleModel.find(filters, fields).sort([(field, 1) for field in fields]))}
        ModuleModel.cache(r_key, result)
    return jsonify(result)
