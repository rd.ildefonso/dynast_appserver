from flask import Blueprint, session, request, current_app as app
from common.utils.jsonify import jsonify
from common.utils.rest import parse_args
from common.flask.decorators import decorator_factory
from models.acl import ACL

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/v1/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def get_one():
    filters, fields = parse_args(request.args)
    return jsonify(ACL.find_one(filters, fields) or {'error': 'Record not found'})


@bp.route('/all', methods=['GET'])
@bp.csrf_protect
def get_all():
    filters, fields = parse_args(request.args)
    filters = filters if filters else {}
    filters['is_viewable'] = True
    return jsonify({'data': list(ACL.find(filters, fields))})
