from flask import session, request
from common.flask.factory import app_factory


def create_app(**kwargs):
    app = app_factory.add_app(__name__, __path__, **kwargs)
    if not app:
        return

    def before_request():
        request.app_name = app.name

    app.before_request(before_request)
