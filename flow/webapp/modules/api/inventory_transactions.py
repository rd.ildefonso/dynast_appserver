from flask import Blueprint, session, request, current_app as app
from common.utils.jsonify import jsonify
from common.utils.rest import parse_args
from common.flask.decorators import decorator_factory
from models.inventory_transaction import InventoryTransaction

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/v1/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def get_one():
    filters, fields = parse_args(request.args)
    if filters and 'raw_material' in filters:
        filters['raw_material.$id'] = InventoryTransaction._id_type(filters.pop('raw_material'))
    if filters and 'manufacturing_order' in filters:
        filters['manufacturing_order.$id'] = InventoryTransaction._id_type(filters.pop('manufacturing_order'))
    raw_material = None
    if fields and 'raw_material' in fields:
        from models.raw_material import RawMaterial
        raw_material = RawMaterial.grab(filters['raw_material.$id'])
    result = InventoryTransaction.find_one(filters, fields) or {'error': 'Record not found'}
    if raw_material:
        return jsonify({**result, **{'raw_material': raw_material}})
    if not result:
        return jsonify(result)
    return jsonify(result)

@bp.route('/all', methods=['GET'])
@bp.csrf_protect
def get_all():
    filters, fields = parse_args(request.args)
    if filters and 'raw_material' in filters:
        filters['raw_material.$id'] = InventoryTransaction._id_type(filters.pop('raw_material'))
    if filters and 'manufacturing_order' in filters:
        filters['manufacturing_order.$id'] = InventoryTransaction._id_type(filters.pop('manufacturing_order'))
    raw_material = None
    if fields and 'raw_material' in fields:
        from models.raw_material import RawMaterial
        raw_material = RawMaterial.grab(filters['raw_material.$id'])
    if raw_material:
        return jsonify({'data': list([{**it, **{'raw_material': raw_material}} for it in InventoryTransaction.find(filters, fields)])})
    return jsonify({'data': list(InventoryTransaction.find(filters, fields))})
