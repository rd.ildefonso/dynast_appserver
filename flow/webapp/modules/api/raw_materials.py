from flask import Blueprint, session, request, current_app as app
from common.utils.jsonify import jsonify
from common.utils.rest import parse_args
from common.flask.decorators import decorator_factory
from models.raw_material import RawMaterial as ModuleModel

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix=f'/v1/{MODULE_NAME}')
decorator_factory.integrate_to_blueprint(bp)


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def get_one():
    filters, fields = parse_args(request.args)
    result = ModuleModel.find_one(filters, fields) or {'error': 'Record not found'}
    if not result:
        return jsonify(result)
    if 'stock_on_hand' in list(fields.keys()):
        from models.inventory_transaction import InventoryTransaction
        result['stock_on_hand'] = InventoryTransaction.get_stock_on_hand(result)
    if 'stock_on_hold' in list(fields.keys()):
        from models.receiving_report import ReceivingReport
        result['stock_on_hold'] = ReceivingReport.get_stock_on_hold(result)
    return jsonify(result)


@bp.route('/all', methods=['GET'])
@bp.csrf_protect
def get_all():
    filters, fields = parse_args(request.args)
    r_key = f'api{request.path}?{request.query_string.decode("utf-8")}'

    if filters and 'category' in filters:
        category = app.cache.lrange('packaging_material_category' if filters['category'] == 'packaging_material' else 'raw_material_category', 0, -1)
        filters['category'] = {'$in': [c.decode('utf-8') for c in category]}

    if ModuleModel.cache_exists(r_key):
        result = ModuleModel.cache_get(r_key)
    else:
        result = {'data': list(ModuleModel.find(filters, fields).sort([(field, 1) for field in fields]))}
        ModuleModel.cache(r_key, result)
    return jsonify(result)
