from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory
from models.client import Client

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)

DATA_COLUMNS = [
    {
        'label': 'Name',
        'property': 'name',
        'regex': True
    },
    {
        'label': 'Type',
        'property': 'business_type'
    },
    {
        'label': 'Primary Number',
        'property': 'primary_contact_number',
        'regex': True
    },
    {
        'label': 'Email',
        'property': 'email_address',
        'regex': True
    },
    {
        'label': 'Last Transaction',
        'property': 'last_transaction'
    }
]


@bp.before_request
def before_request():
    request.mod = MODULE_NAME
    if not session['user'].get('access_levels', {}).get('permissions', {}).get(request.mod, {}).get('read', False):
        if request.referrer:
            return redirect(request.referrer)
        return redirect(url_for('default.default'))


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def index():
    return index_page()


@bp.route('/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def edit(_id):
    return index_page(_id)


def index_page(_id=None):
    record = None
    if _id:
        record = Client.grab(_id)
    crud = {
        'home': url_for('.index'),
        'create': url_for('.add'),
        'read': {
            'many': {
                'url': url_for('.load'),
                'columns': DATA_COLUMNS
            },
            'one': {
                'url': url_for('.details', _id='placeholder')
            }
        },
        'update_record': url_for('.save'),
        'delete': {
            'many': {
                'url': url_for('.delete_many')
            },
            'one': {
                'url': url_for('.delete', _id='placeholder')
            }
        }
    }
    return render_template('admin/clients/index.html', crud=crud, record=record, business_types=app.cache.lrange('client_business_type', 0, -1))


@bp.route('/add', methods=['POST'])
def add():
    try:
        values = request.form.to_dict()
        values['created_by'] = session['user']['_id']
        Client.create(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/load', methods=['POST'])
def load():
    try:
        return jsonify(Client.datatable({**request.form.to_dict(), **{'columns': DATA_COLUMNS}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/<objectid:_id>/details', methods=['GET'])
def details(_id):
    try:
        record = Client.grab(_id)
        if not isinstance(record, Client):
            raise
        from models.user import User
        user = User.grab(record['created_by'].id)
        record['created_by'] = user.human_readable() if user else ''
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    return render_template('admin/clients/details.html', record=record, business_types=app.cache.lrange('client_business_type', 0, -1))


@bp.route('/save', methods=['POST'])
def save():
    try:
        values = request.form.to_dict()
        record = Client.grab(values.pop('_id'))
        if not isinstance(record, Client):
            raise
        record.update(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/<objectid:_id>/delete', methods=['GET'])
def delete(_id):
    try:
        record = Client.grab(_id)
        if not isinstance(record, Client):
            raise
        record.delete()
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/delete', methods=['POST'])
def delete_many():
    pass
