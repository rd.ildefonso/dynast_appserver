from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory
from models.finished_good import FinishedGood

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)

DATA_COLUMNS = [
    {
        'label': 'Product Code',
        'property': 'product_code',
        'regex': True
    },
    {
        'label': 'Description',
        'property': 'description',
        'regex': True
    },
    {
        'label': 'Variant',
        'property': 'variant'
    },
    {
        'label': 'On Hand',
        'property': 'stock_on_hand'
    },
    {
        'label': 'Last Transaction',
        'property': 'last_transaction'
    }
]

STOCK_CARD_DATA_COLUMNS = [
    {
        'label': 'Date',
        'property': 'date_created',
        'regex': True
    },
    {
        'label': 'Type',
        'property': 'type'
    },
    {
        'label': 'In',
        'property': 'quantity'
    },
    {
        'label': 'Out',
        'property': 'quantity'
    },
    {
        'label': 'Location',
        'property': 'location'
    },
    {
        'label': 'Price',
        'property': 'price_per_unit'
    },
    {
        'hidden': True,
        'property': 'created_by'
    },
    {
        'hidden': True,
        'property': 'expiry_date'
    },
    {
        'hidden': True,
        'property': 'manufacturing_date'
    }
]


@bp.before_request
def before_request():
    request.mod = MODULE_NAME
    if not session['user'].get('access_levels', {}).get('permissions', {}).get(request.mod, {}).get('read', False):
        if request.referrer:
            return redirect(request.referrer)
        return redirect(url_for('default.default'))


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def index():
    return index_page()


@bp.route('/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def edit(_id):
    return index_page(_id)


def index_page(_id=None):
    record = None
    formula = None
    if _id:
        record = FinishedGood.grab(_id)
        record['name'] = record['product_code']
        from models.formula import Formula
        formula = Formula.find_one({'product_code': record['product_code']})
    crud = {
        'home': url_for('.index'),
        'read': {
            'many': {
                'url': url_for('.load'),
                'columns': DATA_COLUMNS
            },
            'one': {
                'url': url_for('.details', _id='placeholder')
            }
        }
    }
    return render_template('admin/finished_goods/index.html', crud=crud, record=record, categories=app.cache.lrange('raw_material_category', 0, -1), units=app.cache.lrange('raw_material_unit', 0, -1), formula=formula)


@bp.route('/load', methods=['POST'])
def load():
    try:
        return jsonify(FinishedGood.datatable({**request.form.to_dict(), **{'columns': DATA_COLUMNS}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/<objectid:_id>/details', methods=['GET'])
def details(_id):
    try:
        record = FinishedGood.grab(_id)
        if not isinstance(record, FinishedGood):
            raise
        record['name'] = record['product_code']
        from models.formula import Formula
        formula = Formula.find_one({'product_code': record['product_code']})
        from models.finished_good_transaction import FinishedGoodTransaction
        record['stock_on_hand'] = FinishedGoodTransaction.get_stock_on_hand(record)
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    return render_template('admin/finished_goods/details.html', record=record, categories=app.cache.lrange('raw_material_category', 0, -1), units=app.cache.lrange('raw_material_unit', 0, -1), formula=formula)


@bp.route('/<objectid:_id>/stock_card', methods=['GET'])
def stock_card(_id):
    try:
        record = FinishedGood.grab(_id)
        if not isinstance(record, FinishedGood):
            raise
        from models.finished_good_transaction import FinishedGoodTransaction
        record['stock_on_hand'] = FinishedGoodTransaction.get_stock_on_hand(record)
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    redirect_from = request.args.get('from', 'records')
    return render_template('admin/finished_goods/stock_card.html', record=record, STOCK_CARD_DATA_COLUMNS=STOCK_CARD_DATA_COLUMNS, redirect_from=redirect_from)


@bp.route('/<objectid:_id>/stock_card', methods=['POST'])
def load_stock_card(_id):
    try:
        record = FinishedGood.grab(_id)
        if not isinstance(record, FinishedGood):
            raise
        from models.finished_good_transaction import FinishedGoodTransaction
        return jsonify(FinishedGoodTransaction.datatable({**request.form.to_dict(), **{
                'columns': STOCK_CARD_DATA_COLUMNS,
                'required_filter': {
                    'finished_good.$id': record._id
                },
            }}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/<objectid:_id>/stock_transfer', methods=['GET'])
def stock_transfer(_id):
    try:
        record = FinishedGood.grab(_id)
        if not isinstance(record, FinishedGood):
            raise
        from models.finished_good_transaction import FinishedGoodTransaction
        record['stock_on_hand'] = FinishedGoodTransaction.get_stock_on_hand(record)
        record['last_transaction'] = FinishedGoodTransaction.get_last_transaction(record, humanize=True)
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    redirect_from = request.args.get('from')
    return render_template('admin/finished_goods/stock_transfer.html', record=record, redirect_from=redirect_from)


@bp.route('/<objectid:_id>/create_stock_transfer', methods=['POST'])
def create_stock_transfer(_id):
    try:
        record = FinishedGood.grab(_id)
        if not isinstance(record, FinishedGood):
            raise
        kwargs = request.form.to_dict()
        kwargs['created_by'] = session['user']['_id']
        from models.finished_good_transaction import FinishedGoodTransaction
        kwargs['type'] = 'stock_transfer'
        FinishedGoodTransaction.create(**kwargs)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})
