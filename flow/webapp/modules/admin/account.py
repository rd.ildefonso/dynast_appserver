from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory
from models.user import User

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)


@bp.before_request
def before_request():
    request.mod = MODULE_NAME


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def index():
    return settings()


@bp.route('/settings', methods=['GET'])
@bp.csrf_protect
def settings():
    record = User.grab(session['user'].get('_id'))
    if not isinstance(record, User):
        return redirect(url_for('default.default'))
    return render_template('common/pages/account_settings.html', record=record)


@bp.route('/save', methods=['POST'])
def save():
    try:
        values = request.form.to_dict()
        record = User.grab(values.pop('_id'))
        if not isinstance(record, User):
            raise
        record.update(**values)
        record = User.grab(record._id)
        session['user'] = record.to_json()
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/change_password', methods=['GET'])
@bp.csrf_protect
def change_password():
    record = User.grab(session['user'].get('_id'))
    if not isinstance(record, User):
        return redirect(url_for('default.default'))
    return render_template('common/pages/change_password_via_dashboard.html', record=record)
