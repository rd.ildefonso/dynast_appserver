from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory
from models.formula import Formula
from models.user import User

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)

DATA_COLUMNS = [
    {
        'label': 'Product Code',
        'property': 'product_code',
        'regex': True
    },
    {
        'label': 'Client Name',
        'property': 'client'
    },
    {
        'label': 'Formula Number',
        'property': 'formula_no',
        'regex': True
    },
    {
        'label': 'Description',
        'property': 'description',
        'regex': True
    },
    {
        'label': 'Variant',
        'property': 'variant',
        'regex': True
    },
    {
        'label': 'Date Last Modified',
        'property': 'date_last_modified'
    },
    {
        'label': 'Validity',
        'property': 'valid'
    }
]


@bp.before_request
def before_request():
    request.mod = MODULE_NAME
    if not session['user'].get('access_levels', {}).get('permissions', {}).get(request.mod, {}).get('read', False):
        if request.referrer:
            return redirect(request.referrer)
        return redirect(url_for('default.default'))


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def index():
    return index_page()


@bp.route('/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def edit(_id):
    return index_page(_id)


def index_page(_id=None):
    record = None
    if _id:
        record = Formula.grab(_id)
        record['name'] = '#' + record['product_code']
    crud = {
        'home': url_for('.index'),
        'create': url_for('.add'),
        'read': {
            'many': {
                'url': url_for('.load'),
                'columns': DATA_COLUMNS
            },
            'one': {
                'url': url_for('.details', _id='placeholder')
            }
        },
        'update_record': url_for('.save'),
        'delete': {
            'many': {
                'url': url_for('.delete_many')
            },
            'one': {
                'url': url_for('.delete', _id='placeholder')
            }
        }
    }
    return render_template('admin/formulas/index.html', crud=crud, record=record, units=app.cache.lrange('formula_unit', 0, -1))


@bp.route('/add', methods=['POST'])
def add():
    try:
        values = dict({k: v for k, v in request.form.items() if '-materials-' not in k})

        raw_materials = []
        rm_index = 0
        for item_code, quantity in zip(*[request.form.getlist(k) for k in request.form.keys() if 'raw-materials-' in k]):
            rm_index = rm_index + 1
            raw_materials.append({
                'index': rm_index,
                'item_code': item_code,
                'quantity': quantity
            })
        values['raw_materials'] = raw_materials

        packaging_materials = []
        pm_index = 0
        for item_code, quantity in zip(*[request.form.getlist(k) for k in request.form.keys() if 'packaging-materials-' in k]):
            pm_index = pm_index + 1
            packaging_materials.append({
                'index': pm_index,
                'item_code': item_code,
                'quantity': quantity
            })
        values['packaging_materials'] = packaging_materials

        values['created_by'] = session['user']['_id']
        values['last_modified_by'] = session['user']['_id']
        Formula.create(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/load', methods=['POST'])
def load():
    try:
        return jsonify(Formula.datatable({**request.form.to_dict(), **{'columns': DATA_COLUMNS}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/<objectid:_id>/details', methods=['GET'])
def details(_id):
    try:
        record = Formula.grab(_id)
        if not isinstance(record, Formula):
            raise
        record['name'] = '#' + record['formula_no']
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    user = User.grab(record['last_modified_by'].id)
    record['last_modified_by'] = user.human_readable()
    return render_template('admin/formulas/details.html', record=record, units=app.cache.lrange('formula_unit', 0, -1))


@bp.route('/save', methods=['POST'])
def save():
    try:
        values = dict({k: v for k, v in request.form.items() if '-materials-' not in k})
        record = Formula.grab(values.pop('_id'))
        if not isinstance(record, Formula):
            raise

        raw_materials = []
        rm_index = 0
        for item_code, quantity in zip(*[request.form.getlist(k) for k in request.form.keys() if 'raw-materials-' in k]):
            rm_index = rm_index + 1
            raw_materials.append({
                'index': rm_index,
                'item_code': item_code,
                'quantity': quantity
            })
        values['raw_materials'] = raw_materials

        packaging_materials = []
        pm_index = 0
        for item_code, quantity in zip(*[request.form.getlist(k) for k in request.form.keys() if 'packaging-materials-' in k]):
            pm_index = pm_index + 1
            packaging_materials.append({
                'index': pm_index,
                'item_code': item_code,
                'quantity': quantity
            })
        values['packaging_materials'] = packaging_materials

        values['last_modified_by'] = session['user']['_id']
        record.update(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/<objectid:_id>/delete', methods=['GET'])
def delete(_id):
    try:
        record = Formula.grab(_id)
        if not isinstance(record, Formula):
            raise
        record.delete()
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/<objectid:_id>/duplicate', methods=['POST'])
def duplicate(_id):
    try:
        record = Formula.grab(_id)
        if not isinstance(record, Formula):
            raise Exception('Invalid Record')
        values = dict(record)
        for key in ['_id', 'encoded_to_finished_goods', 'date_last_modified', 'last_modified_by']:
            values.pop(key, None)
        values = {**values, **request.form.to_dict()}
        values['created_by'] = session['user']['_id']
        values['last_modified_by'] = session['user']['_id']
        values['client'] = values['client'].id

        Formula.create(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/delete', methods=['GET'])
def delete_many():
    pass
