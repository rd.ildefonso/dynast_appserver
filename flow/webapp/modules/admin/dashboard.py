from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)


@bp.before_request
def before_request():
    request.mod = MODULE_NAME


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def index():
    return render_template('admin/dashboard/index.html')
