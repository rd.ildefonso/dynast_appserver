import arrow
from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory
from models.receiving_report import ReceivingReport
from models.client import Client
from models.raw_material import RawMaterial
from models.location import Location
from models.delivery_receipt import DeliveryReceipt
from models.user import User

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)

DATA_COLUMNS = [
    {
        'label': 'Item Code',
        'property': 'raw_material',
    },
    {
        'label': 'Description',
        'property': 'raw_material',
    },
    {
        'label': 'Delivery Receipt',
        'property': 'delivery_receipt',
    },
    {
        'label': 'Date Delivered',
        'property': 'date_created',
    },
    {
        'label': 'Quantity',
        'property': 'quantity'
    },
    {
        'label': 'From',
        'property': 'client_from'
    },
    {
        'label': 'For',
        'property': 'client_for'
    },
    {
        'label': 'Status',
        'property': 'status'
    },
    {
        'label': 'Quality',
        'property': 'date_quality'
    },
    {
        'label': 'Approved',
        'property': 'date_approved'
    },
    {
        'label': '',
        'property': 'quality_control_no',
        'hidden': True
    },
    {
        'label': '',
        'property': 'quality_by',
        'hidden': True
    },
    {
        'label': '',
        'property': 'approved_by',
        'hidden': True
    }
]


@bp.before_request
def before_request():
    request.mod = MODULE_NAME
    if not session['user'].get('access_levels', {}).get('permissions', {}).get(request.mod, {}).get('read', False):
        if request.referrer:
            return redirect(request.referrer)
        return redirect(url_for('default.default'))


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def index():
    return index_page()


def index_page():
    crud = {
        'home': url_for('.index'),
        'read': {
            'many': {
                'url': url_for('.load'),
                'columns': DATA_COLUMNS
            }
        }
    }
    return render_template('admin/receiving_reports/index.html', crud=crud)


@bp.route('/load', methods=['POST'])
def load():
    try:
        app.logger.debug(request.form)
        vars = request.form.to_dict()
        if vars['columns[3][search][value]'] != '':
            start, end = vars.pop('columns[3][search][value]').split(',')
            app.logger.debug(f'{start} - {end}')
            return jsonify(ReceivingReport.datatable({**vars, **{'columns': DATA_COLUMNS, 'required_filter':
                {
                    'status': {'$ne': 2},
                    'date_created': {'$gte': arrow.get(start).datetime, '$lt': arrow.get(end).datetime}
                }}}))
        return jsonify(ReceivingReport.datatable({**vars, **{'columns': DATA_COLUMNS, 'required_filter': {'status': {'$ne': 2}}}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/<objectid:_id>/change_status', methods=['POST'])
def change_status(_id):
    try:
        record = ReceivingReport.grab(_id)
        if not isinstance(record, ReceivingReport):
            raise
        date_key, date_value, by = record.change_status(session['user']['_id'], **request.form.to_dict())
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True, date_key: date_value.astimezone(request.default_timezone).strftime(request.datetime_format), 'by': by})


@bp.route('/processed/', methods=['GET'])
@bp.csrf_protect
def processed_index():
    request.sub_mod = 'processed_delivery_receipts'
    record = None
    crud = {
        'home': url_for('.processed_index'),
        'read': {
            'many': {
                'url': url_for('.processed_load'),
                'columns': [
                    {
                        'label': 'Item Code',
                        'property': 'raw_material',
                    },
                    {
                        'label': 'Description',
                        'property': 'raw_material',
                    },
                    {
                        'label': 'Delivery Receipt',
                        'property': 'delivery_receipt',
                    },
                    {
                        'label': 'Quantity',
                        'property': 'quantity'
                    },
                    {
                        'label': 'From',
                        'property': 'client_from'
                    },
                    {
                        'label': 'For',
                        'property': 'client_for'
                    },
                    {
                        'label': 'Quality',
                        'property': 'date_quality'
                    },
                    {
                        'label': 'Approved',
                        'property': 'date_approved'
                    },
                    {
                        'label': '',
                        'property': 'quality_control_no',
                        'hidden': True
                    },
                    {
                        'label': '',
                        'property': 'quality_by',
                        'hidden': True
                    },
                    {
                        'label': '',
                        'property': 'approved_by',
                        'hidden': True
                    }
                ]
            }
        }
    }
    return render_template('admin/receiving_reports/processed_index.html', crud=crud, record=record)


@bp.route('/processed/load', methods=['POST'])
def processed_load():
    try:
        return jsonify(ReceivingReport.datatable({**request.form.to_dict(), **{'columns': DATA_COLUMNS, 'required_filter': {'status': 2}}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})
