import arrow
from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory
from models.delivery_receipt import DeliveryReceipt
from models.receiving_report import ReceivingReport
from models.client import Client

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)

DATA_COLUMNS = [
    {
        'label': 'Delivery Number',
        'property': 'receipt_no',
        'regex': True
    },
    {
        'label': 'Purchase Order Number',
        'property': 'purchase_order_no',
        'regex': True
    },
    {
        'label': 'From',
        'property': 'client_from'
    },
    {
        'label': 'For',
        'property': 'client_for'
    },
    {
        'label': 'Date Delivered',
        'property': 'date_delivered'
    },
    {
        'label': 'Date Created',
        'property': 'date_created'
    },
    {
        'label': 'Status',
        'property': 'status'
    }
]


@bp.before_request
def before_request():
    request.mod = MODULE_NAME
    if not session['user'].get('access_levels', {}).get('permissions', {}).get(request.mod, {}).get('read', False):
        if request.referrer:
            return redirect(request.referrer)
        return redirect(url_for('default.default'))


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def index():
    return index_page()


@bp.route('/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def edit(_id):
    return index_page(_id)


def index_page(_id=None):
    record = None
    if _id:
        record = DeliveryReceipt.grab(_id)
        record['name'] = f'#{record["receipt_no"]}'
    crud = {
        'home': url_for('.index'),
        'create': url_for('.add'),
        'read': {
            'many': {
                'url': url_for('.load'),
                'columns': DATA_COLUMNS
            },
            'one': {
                'url': url_for('.details', _id='placeholder')
            }
        },
        'update_record': url_for('.save'),
        'delete': {
            'many': {
                'url': url_for('.delete_many')
            },
            'one': {
                'url': url_for('.delete', _id='placeholder')
            }
        }
    }
    return render_template('admin/delivery_receipts/index.html', crud=crud, record=record)


@bp.route('/add', methods=['POST'])
def add():
    try:
        values = dict({k: v for k, v in request.form.items() if 'items-' not in k})
        items = []
        for item_code, quantity, price_per_unit, expiry_date, manufacturing_date in zip(*[request.form.getlist(k) for k in request.form.keys() if 'items-' in k]):
            items.append({
                'item_code': item_code,
                'quantity': quantity,
                'price_per_unit': price_per_unit,
                'expiry_date': expiry_date,
                'manufacturing_date': manufacturing_date
            })
        values['items'] = items
        values['created_by'] = session['user']['_id']
        DeliveryReceipt.create(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/load', methods=['POST'])
def load():
    try:
        app.logger.debug(request.form)
        vars = request.form.to_dict()
        if vars['columns[4][search][value]'] != '':
            start, end = vars.pop('columns[4][search][value]').split(',')
            app.logger.debug(f'{start} - {end}')
            vars['required_filter'] = {'date_delivered': {'$gte': arrow.get(start).datetime, '$lt': arrow.get(end).datetime}}
        return jsonify(DeliveryReceipt.datatable({**vars, **{'columns': DATA_COLUMNS}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/<objectid:_id>/details', methods=['GET'])
def details(_id):
    try:
        record = DeliveryReceipt.grab(_id)
        if not isinstance(record, DeliveryReceipt):
            raise
        record['name'] = f'#{record["receipt_no"]}'
        from models.user import User
        user = User.grab(record['created_by'].id)
        record['created_by'] = user.human_readable() if user else ''
        if 'submitted_by' in record:
            user = User.grab(record['submitted_by'].id)
            record['submitted_by'] = user.human_readable() if user else ''
        if 'completed_by' in record:
            user = User.grab(record['completed_by'].id)
            record['completed_by'] = user.human_readable() if user else ''
        if record['status'] != 0:
            items = record['items']
            from models.raw_material import RawMaterial
            for index, item in enumerate(items):
                raw_material = RawMaterial.grab(item['item_code'])
                receiving_report = ReceivingReport.find_one({'delivery_receipt.$id': record._id, 'raw_material.$id': raw_material._id})
                record['items'][index] = {**item, **dict(receiving_report), **dict(raw_material)}
                if 'location' in receiving_report:
                    from models.location import Location
                    record['items'][index]['location'] = dict(Location.grab(receiving_report['location'].id))
            record['items'] = items
            record['client_from'] = Client.grab(record['client_from'].id).name
            record['client_for'] = Client.grab(record['client_for'].id).name
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    return render_template('admin/delivery_receipts/details.html', record=record, editable=record['status'] == 0)


@bp.route('/save', methods=['POST'])
def save():
    try:
        values = dict({k: v for k, v in request.form.items() if 'items-' not in k})
        record = DeliveryReceipt.grab(values.pop('_id'))
        if not isinstance(record, DeliveryReceipt):
            raise
        items = []
        for item_code, quantity, price_per_unit, expiry_date, manufacturing_date in zip(*[request.form.getlist(k) for k in request.form.keys() if 'items-' in k]):
            items.append({
                'item_code': item_code,
                'quantity': quantity,
                'price_per_unit': price_per_unit,
                'expiry_date': expiry_date,
                'manufacturing_date': manufacturing_date
            })
        values['items'] = items
        record.update(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/<objectid:_id>/delete', methods=['GET'])
def delete(_id):
    try:
        record = DeliveryReceipt.grab(_id)
        if not isinstance(record, DeliveryReceipt):
            raise
        record.delete()
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/<objectid:_id>/change_status', methods=['POST'])
def change_status(_id):
    try:
        record = DeliveryReceipt.grab(_id)
        if not isinstance(record, DeliveryReceipt):
            raise
        record.change_status(session['user']['_id'], **request.form.to_dict())
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/delete', methods=['GET'])
def delete_many():
    pass
