from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory
from models.outgoing_delivery import OutgoingDelivery

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)

DATA_COLUMNS = [
    {
        'label': 'Outgoing Delivery Number',
        'property': 'outgoing_delivery_no',
        'regex': True
    },
    {
        'label': 'Client',
        'property': 'client'
    },
    {
        'label': 'Date Created',
        'property': 'date_created'
    },
    {
        'label': 'Status',
        'property': 'status'
    },
    {
        'hidden': True,
        'property': 'date_reserved'
    },
    {
        'hidden': True,
        'property': 'date_paid'
    },
    {
        'hidden': True,
        'property': 'date_transit'
    },
    {
        'hidden': True,
        'property': 'date_completed'
    }
]


@bp.before_request
def before_request():
    request.mod = MODULE_NAME
    if not session['user'].get('access_levels', {}).get('permissions', {}).get(request.mod, {}).get('read', False):
        if request.referrer:
            return redirect(request.referrer)
        return redirect(url_for('default.default'))


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def index():
    return index_page()


@bp.route('/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def edit(_id):
    return index_page(_id)


def index_page(_id=None):
    record = None
    if _id:
        record = None
        record = OutgoingDelivery.grab(_id)
        record['name'] = f'#{record["outgoing_delivery_no"]}'
    crud = {
        'home': url_for('.index'),
        'create': url_for('.add'),
        'read': {
            'many': {
                'url': url_for('.load'),
                'columns': DATA_COLUMNS
            },
            'one': {
                'url': url_for('.details', _id='placeholder')
            }
        },
        'update_record': url_for('.save'),
        'delete': {
            'many': {
                'url': url_for('.delete_many')
            },
            'one': {
                'url': url_for('.delete', _id='placeholder')
            }
        }
    }
    return render_template('admin/outgoing_deliveries/index.html', crud=crud, record=record)


@bp.route('/add', methods=['POST'])
def add():
    try:
        values = dict({k: v for k, v in request.form.items() if 'products-' not in k})
        products = []
        for product_code, quantity, price_per_unit in zip(*[request.form.getlist(k) for k in request.form.keys() if 'products-' in k]):
            products.append({
                'product_code': product_code,
                'quantity': quantity,
                'price_per_unit': price_per_unit
            })
        values['products'] = products
        values['created_by'] = session['user']['_id']
        OutgoingDelivery.create(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/load', methods=['POST'])
def load():
    try:
        return jsonify(OutgoingDelivery.datatable({**request.form.to_dict(), **{'columns': DATA_COLUMNS}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/<objectid:_id>/details', methods=['GET'])
def details(_id):
    try:
        record = OutgoingDelivery.grab(_id)
        if not isinstance(record, OutgoingDelivery):
            raise
        record['name'] = f'#{record["outgoing_delivery_no"]}'
        from models.user import User
        for category in ['created_by', 'reserved_by', 'paid_by', 'transit_by', 'completed_by']:
            if record.get(category):
                user = User.grab(record[category].id)
                record[category] = user.human_readable()
            else:
                record[category] = ''
        from models.client import Client
        record['client'] = Client.grab(record['client'].id)
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    return render_template('admin/outgoing_deliveries/details.html', record=record, editable=record['status'] == 0)


@bp.route('/save', methods=['POST'])
def save():
    try:
        values = dict({k: v for k, v in request.form.items() if 'products-' not in k})
        record = OutgoingDelivery.grab(values.pop('_id'))
        if not isinstance(record, OutgoingDelivery):
            raise
        products = []
        for product_code, quantity, price_per_unit in zip(*[request.form.getlist(k) for k in request.form.keys() if 'products-' in k]):
            products.append({
                'product_code': product_code,
                'quantity': quantity,
                'price_per_unit': price_per_unit
            })
        values['products'] = products
        record.update(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/<objectid:_id>/delete', methods=['GET'])
def delete(_id):
    try:
        record = OutgoingDelivery.grab(_id)
        if not isinstance(record, OutgoingDelivery):
            raise
        record.delete()
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/delete', methods=['GET'])
def delete_many():
    pass
