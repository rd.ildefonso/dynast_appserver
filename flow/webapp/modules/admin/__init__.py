from flask import session, request, flash, redirect, url_for
from common.flask.factory import app_factory
from settings import DEVELOPMENT_MODE


def create_app(**kwargs):
    app = app_factory.add_app(__name__, __path__, **kwargs)
    if not app:
        return

    def before_request():
        request.app_name = app.name
        request.datetime_format = app.datetime_format
        request.default_timezone = app.default_timezone
        request.debug = app.debug
        if request.endpoint not in ['default.default', 'login.form', 'login.submit', 'login.change_password', 'login.change_password_submit', 'static', None]:
            if 'user' not in session:
                if DEVELOPMENT_MODE:
                    from models.user import SuperAdmin
                    session['user'] = SuperAdmin.check().to_json()
                else:
                    app.logger.warning('No Session Found for %s', request)
                    flash('No Session Found')
                    return redirect(url_for('login.form'))
            if session['user'].get('reset_password'):
                flash('Change Password to Continue')
                return redirect(url_for('login.change_password'))
            if not isinstance(session['user'].get('access_levels'), dict):
                flash('Invalid Permissions (01)')
                return redirect(url_for('login.form'))
            if not isinstance(session['user'].get('access_levels', {}).get('permissions'), dict):
                flash('Invalid Permissions (02)')
                return redirect(url_for('login.form'))
    app.before_request(before_request)
