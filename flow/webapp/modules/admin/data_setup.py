from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory
from models.data_setup import RawMaterialCategory, PackagingMaterialCategory, RawMaterialUnit, ClientBusinessType, FormulaUnit

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)

SELECT_TYPE_DATA_COLUMNS = [
        {
            'label': 'Value',
            'property': 'value'
        },
]


@bp.before_request
def before_request():
    request.mod = MODULE_NAME
    if not session['user'].get('access_levels', {}).get('permissions', {}).get(request.mod, {}).get('read', False):
        if request.referrer:
            return redirect(request.referrer)
        return redirect(url_for('default.default'))


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def default():
    return redirect(url_for('.raw_material_categories_index'))


# SECTION: Raw Material Categories
@bp.route('/raw_material_categories/', methods=['GET'])
@bp.csrf_protect
def raw_material_categories_index():
    return raw_material_categories_index_page()


@bp.route('/raw_material_categories/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def raw_material_categories_edit(_id):
    return raw_material_categories_index_page(_id)


def raw_material_categories_index_page(_id=None):
    request.sub_mod = 'raw_material_categories'
    record = None
    if _id:
        record = RawMaterialCategory.grab(_id)
    crud = {
        'create': url_for('.raw_material_categories_add'),
        'read': {
            'many': {
                'url': url_for('.raw_material_categories_load'),
                'columns': SELECT_TYPE_DATA_COLUMNS
            },
            'one': {
                'url': url_for('.raw_material_categories_details', _id='placeholder')
            }
        },
        'update_record': url_for('.raw_material_categories_save'),
        'delete': {
            'many': {
                'url': url_for('.raw_material_categories_delete_many')
            },
            'one': {
                'url': url_for('.raw_material_categories_delete', _id='placeholder')
            }
        }
    }
    return render_template('admin/data_setup/select/index.html', crud=crud, record=record)


@bp.route('/raw_material_categories/add', methods=['POST'])
def raw_material_categories_add():
    try:
        RawMaterialCategory.create(**request.form.to_dict())
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/raw_material_categories/load', methods=['POST'])
def raw_material_categories_load():
    try:
        return jsonify(RawMaterialCategory.datatable({**request.form.to_dict(), **{'columns': SELECT_TYPE_DATA_COLUMNS}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/raw_material_categories/<objectid:_id>/details', methods=['GET'])
def raw_material_categories_details(_id):
    try:
        record = RawMaterialCategory.grab(_id)
        if not isinstance(record, RawMaterialCategory):
            raise
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    return render_template('admin/data_setup/select/details.html', record=record)


@bp.route('/raw_material_categories/save', methods=['POST'])
def raw_material_categories_save():
    try:
        record = RawMaterialCategory.grab(request.form.get('_id'))
        if not isinstance(record, RawMaterialCategory):
            raise
        kwargs = request.form.to_dict()
        kwargs.pop('_id')
        record.update(**kwargs)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/raw_material_categories/<objectid:_id>/delete', methods=['GET'])
def raw_material_categories_delete(_id):
    try:
        record = RawMaterialCategory.grab(_id)
        if not isinstance(record, RawMaterialCategory):
            raise
        record.delete()
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('raw_material_categories/delete', methods=['POST'])
def raw_material_categories_delete_many():
    pass


# SECTION: Packaging Material Categories
@bp.route('/packaging_material_categories/', methods=['GET'])
@bp.csrf_protect
def packaging_material_categories_index():
    return packaging_material_categories_index_page()


@bp.route('/packaging_material_categories/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def packaging_material_categories_edit(_id):
    return packaging_material_categories_index_page(_id)


def packaging_material_categories_index_page(_id=None):
    request.sub_mod = 'packaging_material_categories'
    record = None
    if _id:
        record = PackagingMaterialCategory.grab(_id)
    crud = {
        'create': url_for('.packaging_material_categories_add'),
        'read': {
            'many': {
                'url': url_for('.packaging_material_categories_load'),
                'columns': SELECT_TYPE_DATA_COLUMNS
            },
            'one': {
                'url': url_for('.packaging_material_categories_details', _id='placeholder')
            }
        },
        'update_record': url_for('.packaging_material_categories_save'),
        'delete': {
            'many': {
                'url': url_for('.packaging_material_categories_delete_many')
            },
            'one': {
                'url': url_for('.packaging_material_categories_delete', _id='placeholder')
            }
        }
    }
    return render_template('admin/data_setup/select/index.html', crud=crud, record=record)


@bp.route('/packaging_material_categories/add', methods=['POST'])
def packaging_material_categories_add():
    try:
        PackagingMaterialCategory.create(**request.form.to_dict())
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/packaging_material_categories/load', methods=['POST'])
def packaging_material_categories_load():
    try:
        return jsonify(PackagingMaterialCategory.datatable({**request.form.to_dict(), **{'columns': SELECT_TYPE_DATA_COLUMNS}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/packaging_material_categories/<objectid:_id>/details', methods=['GET'])
def packaging_material_categories_details(_id):
    try:
        record = PackagingMaterialCategory.grab(_id)
        if not isinstance(record, PackagingMaterialCategory):
            raise
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    return render_template('admin/data_setup/select/details.html', record=record)


@bp.route('/packaging_material_categories/save', methods=['POST'])
def packaging_material_categories_save():
    try:
        record = PackagingMaterialCategory.grab(request.form.get('_id'))
        if not isinstance(record, PackagingMaterialCategory):
            raise
        kwargs = request.form.to_dict()
        kwargs.pop('_id')
        record.update(**kwargs)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/packaging_material_categories/<objectid:_id>/delete', methods=['GET'])
def packaging_material_categories_delete(_id):
    try:
        record = PackagingMaterialCategory.grab(_id)
        if not isinstance(record, PackagingMaterialCategory):
            raise
        record.delete()
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('packaging_material_categories/delete', methods=['POST'])
def packaging_material_categories_delete_many():
    pass


# SECTION: Raw Material Units
@bp.route('/raw_material_units/', methods=['GET'])
@bp.csrf_protect
def raw_material_units_index():
    return raw_material_units_index_page()


@bp.route('/raw_material_units/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def raw_material_units_edit(_id):
    return raw_material_units_index_page(_id)


def raw_material_units_index_page(_id=None):
    request.sub_mod = 'raw_material_units'
    record = None
    if _id:
        record = RawMaterialUnit.grab(_id)
    crud = {
        'create': url_for('.raw_material_units_add'),
        'read': {
            'many': {
                'url': url_for('.raw_material_units_load'),
                'columns': SELECT_TYPE_DATA_COLUMNS
            },
            'one': {
                'url': url_for('.raw_material_units_details', _id='placeholder')
            }
        },
        'update_record': url_for('.raw_material_units_save'),
        'delete': {
            'many': {
                'url': url_for('.raw_material_units_delete_many')
            },
            'one': {
                'url': url_for('.raw_material_units_delete', _id='placeholder')
            }
        }
    }
    return render_template('admin/data_setup/select/index.html', crud=crud, record=record)


@bp.route('/raw_material_units/add', methods=['POST'])
def raw_material_units_add():
    try:
        RawMaterialUnit.create(**request.form.to_dict())
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/raw_material_units/load', methods=['POST'])
def raw_material_units_load():
    try:
        return jsonify(RawMaterialUnit.datatable({**request.form.to_dict(), **{'columns': SELECT_TYPE_DATA_COLUMNS}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/raw_material_units/<objectid:_id>/details', methods=['GET'])
def raw_material_units_details(_id):
    try:
        record = RawMaterialUnit.grab(_id)
        if not isinstance(record, RawMaterialUnit):
            raise
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    return render_template('admin/data_setup/select/details.html', record=record)


@bp.route('/raw_material_units/save', methods=['POST'])
def raw_material_units_save():
    try:
        record = RawMaterialUnit.grab(request.form.get('_id'))
        if not isinstance(record, RawMaterialUnit):
            raise
        kwargs = request.form.to_dict()
        kwargs.pop('_id')
        record.update(**kwargs)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/raw_material_units/<objectid:_id>/delete', methods=['GET'])
def raw_material_units_delete(_id):
    try:
        record = RawMaterialUnit.grab(_id)
        if not isinstance(record, RawMaterialUnit):
            raise
        record.delete()
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('raw_material_units/delete', methods=['POST'])
def raw_material_units_delete_many():
    pass


# SECTION: Client Business Types
@bp.route('/client_business_types/', methods=['GET'])
@bp.csrf_protect
def client_business_types_index():
    return client_business_types_index_page()


@bp.route('/client_business_types/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def client_business_types_edit(_id):
    return client_business_types_index_page(_id)


def client_business_types_index_page(_id=None):
    request.sub_mod = 'client_business_types'
    record = None
    if _id:
        record = ClientBusinessType.grab(_id)
    crud = {
        'create': url_for('.client_business_types_add'),
        'read': {
            'many': {
                'url': url_for('.client_business_types_load'),
                'columns': SELECT_TYPE_DATA_COLUMNS
            },
            'one': {
                'url': url_for('.client_business_types_details', _id='placeholder')
            }
        },
        'update_record': url_for('.client_business_types_save'),
        'delete': {
            'many': {
                'url': url_for('.client_business_types_delete_many')
            },
            'one': {
                'url': url_for('.client_business_types_delete', _id='placeholder')
            }
        }
    }
    return render_template('admin/data_setup/select/index.html', crud=crud, record=record)


@bp.route('/client_business_types/add', methods=['POST'])
def client_business_types_add():
    try:
        ClientBusinessType.create(**request.form.to_dict())
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/client_business_types/load', methods=['POST'])
def client_business_types_load():
    try:
        return jsonify(ClientBusinessType.datatable({**request.form.to_dict(), **{'columns': SELECT_TYPE_DATA_COLUMNS}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/client_business_types/<objectid:_id>/details', methods=['GET'])
def client_business_types_details(_id):
    try:
        record = ClientBusinessType.grab(_id)
        if not isinstance(record, ClientBusinessType):
            raise
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    return render_template('admin/data_setup/select/details.html', record=record)


@bp.route('/client_business_types/save', methods=['POST'])
def client_business_types_save():
    try:
        record = ClientBusinessType.grab(request.form.get('_id'))
        if not isinstance(record, ClientBusinessType):
            raise
        kwargs = request.form.to_dict()
        kwargs.pop('_id')
        record.update(**kwargs)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/client_business_types/<objectid:_id>/delete', methods=['GET'])
def client_business_types_delete(_id):
    try:
        record = ClientBusinessType.grab(_id)
        if not isinstance(record, ClientBusinessType):
            raise
        record.delete()
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('client_business_types/delete', methods=['POST'])
def client_business_types_delete_many():
    pass


# SECTION: Formula Units
@bp.route('/formula_units/', methods=['GET'])
@bp.csrf_protect
def formula_units_index():
    return formula_units_index_page()


@bp.route('/formula_units/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def formula_units_edit(_id):
    return formula_units_index_page(_id)


def formula_units_index_page(_id=None):
    request.sub_mod = 'formula_units'
    record = None
    if _id:
        record = FormulaUnit.grab(_id)
    crud = {
        'create': url_for('.formula_units_add'),
        'read': {
            'many': {
                'url': url_for('.formula_units_load'),
                'columns': SELECT_TYPE_DATA_COLUMNS
            },
            'one': {
                'url': url_for('.formula_units_details', _id='placeholder')
            }
        },
        'update_record': url_for('.formula_units_save'),
        'delete': {
            'many': {
                'url': url_for('.formula_units_delete_many')
            },
            'one': {
                'url': url_for('.formula_units_delete', _id='placeholder')
            }
        }
    }
    return render_template('admin/data_setup/select/index.html', crud=crud, record=record)


@bp.route('/formula_units/add', methods=['POST'])
def formula_units_add():
    try:
        FormulaUnit.create(**request.form.to_dict())
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/formula_units/load', methods=['POST'])
def formula_units_load():
    try:
        return jsonify(FormulaUnit.datatable({**request.form.to_dict(), **{'columns': SELECT_TYPE_DATA_COLUMNS}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/formula_units/<objectid:_id>/details', methods=['GET'])
def formula_units_details(_id):
    try:
        record = FormulaUnit.grab(_id)
        if not isinstance(record, FormulaUnit):
            raise
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    return render_template('admin/data_setup/select/details.html', record=record)


@bp.route('/formula_units/save', methods=['POST'])
def formula_units_save():
    try:
        record = FormulaUnit.grab(request.form.get('_id'))
        if not isinstance(record, FormulaUnit):
            raise
        kwargs = request.form.to_dict()
        kwargs.pop('_id')
        record.update(**kwargs)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/formula_units/<objectid:_id>/delete', methods=['GET'])
def formula_units_delete(_id):
    try:
        record = FormulaUnit.grab(_id)
        if not isinstance(record, FormulaUnit):
            raise
        record.delete()
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('formula_units/delete', methods=['POST'])
def formula_units_delete_many():
    pass
