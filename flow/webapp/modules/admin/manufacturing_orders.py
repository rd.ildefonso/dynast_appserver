from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory
from models.manufacturing_order import ManufacturingOrder
from models.formula import Formula

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)

DATA_COLUMNS = [
    {
        'label': 'MO Number',
        'property': 'manufacturing_order_no'
    },
    {
        'label': 'Product',
        'property': 'formula_no'
    },
    {
        'label': 'Batch Size',
        'property': 'batch_size'
    },
    {
        'label': 'Date Created',
        'property': 'date_created'
    },
    {
        'label': 'Status',
        'property': 'status'
    }
]


@bp.before_request
def before_request():
    request.mod = MODULE_NAME
    if not session['user'].get('access_levels', {}).get('permissions', {}).get(request.mod, {}).get('read', False):
        if request.referrer:
            return redirect(request.referrer)
        return redirect(url_for('default.default'))


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def index():
    return index_page()


@bp.route('/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def edit(_id):
    return index_page(_id)


def index_page(_id=None):
    record = None
    if _id:
        record = ManufacturingOrder.grab(_id)
        record['name'] = f'#{record["manufacturing_order_no"]}'
    crud = {
        'home': url_for('.index'),
        'create': url_for('.add'),
        'read': {
            'many': {
                'url': url_for('.load'),
                'columns': DATA_COLUMNS
            },
            'one': {
                'url': url_for('.details', _id='placeholder')
            }
        },
        'update_record': url_for('.save'),
        'delete': {
            'many': {
                'url': url_for('.delete_many')
            },
            'one': {
                'url': url_for('.delete', _id='placeholder')
            }
        }
    }
    return render_template('admin/manufacturing_orders/index.html', crud=crud, record=record)


@bp.route('/add', methods=['POST'])
def add():
    try:
        values = request.form.to_dict()
        values['created_by'] = session['user']['_id']
        ManufacturingOrder.create(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/load', methods=['POST'])
def load():
    try:
        return jsonify(ManufacturingOrder.datatable({**request.form.to_dict(), **{'columns': DATA_COLUMNS}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/<objectid:_id>/details', methods=['GET'])
def details(_id):
    try:
        record = ManufacturingOrder.grab(_id)
        if not isinstance(record, ManufacturingOrder):
            raise
        # TODO
        # if record['status'] != 0:
        #     formula = Formula.grab(record.formula_no.id)
        #     record['formula_no'] = f'{formula.product_code} - {formula.description} {formula.variant}'
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    return render_template('admin/manufacturing_orders/details.html', record=record, editable=record['status'] == 0)


@bp.route('/save', methods=['POST'])
def save():
    try:
        record = ManufacturingOrder.grab(request.form.get('_id'))
        if not isinstance(record, ManufacturingOrder):
            raise
        kwargs = request.form.to_dict()
        kwargs.pop('_id')
        record.update(**kwargs)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/<objectid:_id>/delete', methods=['GET'])
def delete(_id):
    try:
        record = ManufacturingOrder.grab(_id)
        if not isinstance(record, ManufacturingOrder):
            raise
        record.delete()
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/<objectid:_id>/change_status', methods=['POST'])
def change_status(_id):
    try:
        record = ManufacturingOrder.grab(_id)
        if not isinstance(record, ManufacturingOrder):
            raise
        status = int(request.form.get('status'))
        record.change_status(status, session['user']['_id'])
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/<objectid:_id>/print_summary', methods=['GET'])
def print_summary(_id):
    result = None
    from flask_weasyprint import HTML, render_pdf
    try:
        record = ManufacturingOrder.grab(_id)
        if not isinstance(record, ManufacturingOrder):
            raise
        formula = Formula.grab(record.formula_no.id)
        if not isinstance(formula, Formula):
            raise
        rows = {}
        from models.inventory_transaction import InventoryTransaction
        from models.raw_material import RawMaterial
        for category in ['raw_materials', 'packaging_materials']:
            rows[category] = []
            materials = formula[category]
            for material in materials:
                rm = RawMaterial.grab(material['item_code'])
                it = InventoryTransaction.find_one({'raw_material.$id': rm._id, 'manufacturing_order.$id': record._id, 'type': 'used'})
                rows[category].append({
                    'item_code': rm.item_code,
                    'description': rm.description,
                    'default_unit': rm.default_unit,
                    'cumulative_quantity': round(abs(it.cumulative_quantity), 6),
                    'qc_nos': it.used_quality_control_nos
                })
        result = render_pdf(HTML(string=render_template('admin/manufacturing_orders/pdfs/summary.html', record=record, formula=formula, rows=rows)))
    except Exception as e:
        app.logger.exception(e)
        result = render_pdf(render_template('common/pdfs/error.html', error=e))
    return result


@bp.route('/<objectid:_id>/print_picking_slip', methods=['GET'])
def print_picking_slip(_id):
    result = None
    from flask_weasyprint import HTML, render_pdf
    try:
        record = ManufacturingOrder.grab(_id)
        if not isinstance(record, ManufacturingOrder):
            raise
        formula = Formula.grab(record.formula_no.id)
        if not isinstance(formula, Formula):
            raise
        rows = {}
        from models.inventory_transaction import InventoryTransaction
        from models.raw_material import RawMaterial
        for category in ['raw_materials', 'packaging_materials']:
            rows[category] = []
            materials = formula[category]
            for material in materials:
                rm = RawMaterial.grab(material['item_code'])
                it = InventoryTransaction.find_one({'raw_material.$id': rm._id, 'manufacturing_order.$id': record._id, 'type': 'used'})
                rows[category].append({
                    'item_code': rm.item_code,
                    'description': rm.description,
                    'default_unit': rm.default_unit,
                    'cumulative_quantity': round(abs(it.cumulative_quantity), 4),
                    'qc_nos': it.used_quality_control_nos
                })
        result = render_pdf(HTML(string=render_template('admin/manufacturing_orders/pdfs/picking_slip.html', record=record, formula=formula, rows=rows)))
    except Exception as e:
        app.logger.exception(e)
        result = render_pdf(render_template('common/pdfs/error.html', error=e))
    return result


@bp.route('/delete', methods=['GET'])
def delete_many():
    pass
