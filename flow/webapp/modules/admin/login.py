from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory
from models.user import User

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)


@bp.before_request
def before_request():
    request.mod = MODULE_NAME


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def form():
    if 'user' in session:
        session.clear()
    # if request.is_xhr:
    #     flash('Session expired')
    return render_template('common/pages/login.html')


@bp.route('/', methods=['POST'])
@bp.csrf_protect
def submit():
    username = request.form.get('username', '')
    password = request.form.get('password', '')

    valid, output = User.authenticate(username, password)
    if valid:
        user = output.to_json()
        session['user'] = user
        return redirect(url_for('dashboard.index'))
    flash(output)
    return redirect(url_for('login.form'))


@bp.route('/change_password', methods=['GET'])
@bp.csrf_protect
def change_password():
    if 'user' not in session:
        flash('Session has Expired')
        return redirect(url_for('login.form'))
    return render_template('common/pages/change_password.html')


@bp.route('/change_password', methods=['POST'])
@bp.csrf_protect
def change_password_submit():
    username = request.form.get('username', '')
    current_password = request.form.get('current_password', '')
    target_password = request.form.get('target_password', '')

    valid, output = User.authenticate(username, current_password)
    if valid:
        try:
            output.update(password=target_password, reset_password=False)
            session['user']['reset_password'] = False
        except Exception as e:
            app.logger.exception(e)
            flash(f'Error - {e}')
            return redirect(url_for('login.change_password'))
        return redirect(url_for('dashboard.index'))
    else:
        flash('Password Invalid')
        return redirect(url_for('login.change_password'))
