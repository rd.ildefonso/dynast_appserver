from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory
from models.forecast import Forecast
from models.user import User

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)

DATA_COLUMNS = [
    {
        'label': 'Forecast Number',
        'property': 'forecast_no',
    },
    {
        'label': 'Date Last Modified',
        'property': 'date_last_modified'
    },
    {
        'label': 'Last Modified By',
        'property': 'last_modified_by'
    },
]


@bp.before_request
def before_request():
    request.mod = MODULE_NAME
    if not session['user'].get('access_levels', {}).get('permissions', {}).get(request.mod, {}).get('read', False):
        if request.referrer:
            return redirect(request.referrer)
        return redirect(url_for('default.default'))


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def index():
    return index_page()


@bp.route('/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def edit(_id):
    return index_page(_id)


def index_page(_id=None):
    record = None
    if _id:
        record = Forecast.grab(_id)
        record['name'] = f'#{record["forecast_no"]}'
    crud = {
        'home': url_for('.index'),
        'create': url_for('.add'),
        'read': {
            'many': {
                'url': url_for('.load'),
                'columns': DATA_COLUMNS
            },
            'one': {
                'url': url_for('.details', _id='placeholder')
            }
        },
        'update_record': url_for('.save'),
        'delete': {
            'many': {
                'url': url_for('.delete_many')
            },
            'one': {
                'url': url_for('.delete', _id='placeholder')
            }
        }
    }
    return render_template('admin/forecasts/index.html', crud=crud, record=record)


@bp.route('/add', methods=['POST'])
def add():
    try:
        values = {}

        finished_goods = []
        fg_index = 0
        for formula_no, bulk_kgs, bulk_pcs in zip(*[request.form.getlist(k) for k in request.form.keys() if 'finished-goods-' in k]):
            fg_index = fg_index + 1
            finished_goods.append({
                'index': fg_index,
                'formula_no': formula_no,
                'bulk_kgs': bulk_kgs,
                'bulk_quantity': bulk_pcs
            })
        values['finished_goods'] = finished_goods

        values['created_by'] = session['user']['_id']
        values['last_modified_by'] = session['user']['_id']
        Forecast.create(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/load', methods=['POST'])
def load():
    try:
        return jsonify(Forecast.datatable({**request.form.to_dict(), **{'columns': DATA_COLUMNS}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/<objectid:_id>/details', methods=['GET'])
def details(_id):
    try:
        record = Forecast.grab(_id)
        if not isinstance(record, Forecast):
            raise
        record['name'] = '#' + record['forecast_no']
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    user = User.grab(record['last_modified_by'].id)
    record['last_modified_by'] = user.human_readable() if user else ''
    return render_template('admin/forecasts/details.html', record=record)


@bp.route('/save', methods=['POST'])
def save():
    try:
        values = dict({k: v for k, v in request.form.items() if 'finished-goods-' not in k})
        record = Forecast.grab(values.pop('_id'))
        if not isinstance(record, Forecast):
            raise

        finished_goods = []
        fg_index = 0
        for formula_no, quantity, bulk in zip(*[request.form.getlist(k) for k in request.form.keys() if 'finished-goods-' in k]):
            fg_index = fg_index + 1
            finished_goods.append({
                'index': fg_index,
                'formula_no': formula_no,
                'quantity': quantity,
                'bulk': bulk
            })
        values['finished_goods'] = finished_goods

        values['last_modified_by'] = session['user']['_id']
        record.update(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/<objectid:_id>/delete', methods=['GET'])
def delete(_id):
    try:
        record = Forecast.grab(_id)
        if not isinstance(record, Forecast):
            raise
        record.delete()
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/delete', methods=['POST'])
def delete_many():
    pass
