from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory
from models.user import User
from models.acl import ACL

MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)

DATA_COLUMNS = [
    {
        'label': 'Username',
        'property': 'username',
        'regex': True
    },
    {
        'label': 'First Name',
        'property': 'first_name',
        'regex': True
    },
    {
        'label': 'Last Name',
        'property': 'last_name',
        'regex': True
    },
    {
        'label': 'Access Level',
        'property': 'access_levels'
    },
    {
        'label': 'Primary Contact Number',
        'property': 'primary_contact_number',
        'regex': True
    },
    {
        'label': 'Date Created',
        'property': 'date_created'
    },
    {
        'label': '',
        'property': 'status'
    },
]


@bp.before_request
def before_request():
    request.mod = MODULE_NAME
    if not session['user'].get('access_levels', {}).get('permissions', {}).get(request.mod, {}).get('read', False):
        if request.referrer:
            return redirect(request.referrer)
        return redirect(url_for('default.default'))


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def index():
    return index_page()


@bp.route('/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def edit(_id):
    return index_page(_id)


def index_page(_id=None):
    record = None
    if _id:
        record = User.grab(_id)
    crud = {
        'home': url_for('.index'),
        'create': url_for('.add'),
        'read': {
            'many': {
                'url': url_for('.load'),
                'columns': DATA_COLUMNS
            },
            'one': {
                'url': url_for('.details', _id='placeholder')
            }
        },
        'update_record': url_for('.save')
    }
    return render_template('admin/users/index.html', crud=crud, record=record)


@bp.route('/add', methods=['POST'])
def add():
    try:
        values = request.form.to_dict()
        values['created_by'] = session['user']['_id']
        values['reset_password'] = True if values['reset_password'].lower() == 'true' else False
        User.create(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/load', methods=['POST'])
def load():
    try:
        return jsonify(User.datatable({**request.form.to_dict(), **{'columns': DATA_COLUMNS, 'required_filter': {'_id': {'$ne': User._id_type(session['user']['_id'])}}}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/<objectid:_id>/details', methods=['GET'])
def details(_id):
    try:
        record = User.grab(_id)
        if not isinstance(record, User):
            raise
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    return render_template('admin/users/details.html', record=record)


@bp.route('/save', methods=['POST'])
def save():
    try:
        values = request.form.to_dict()
        record = User.grab(values.pop('_id'))
        if not isinstance(record, User):
            raise
        values['reset_password'] = True if values['reset_password'].lower() == 'true' else False
        record.update(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/<objectid:_id>/reset_password', methods=['POST'])
def reset_password(_id):
    try:
        values = request.form.to_dict()
        record = User.grab(_id)
        if not isinstance(record, User):
            raise
        values['reset_password'] = True
        record.update(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/<objectid:_id>/change_status', methods=['POST'])
def change_status(_id):
    try:
        values = request.form.to_dict()
        record = User.grab(_id)
        if not isinstance(record, User):
            raise
        values['status'] = int(values.get('status', -1))
        record.update(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})
