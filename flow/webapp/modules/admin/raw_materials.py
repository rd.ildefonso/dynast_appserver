from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
from common.utils.jsonify import jsonify
from common.flask.decorators import decorator_factory
from models.raw_material import RawMaterial

MODULE_NAME = 'materials'
bp = Blueprint(MODULE_NAME, __name__, url_prefix='/%s' % (MODULE_NAME))
decorator_factory.integrate_to_blueprint(bp)

DATA_COLUMNS = [
    {
        'label': 'Item Code',
        'property': 'item_code',
        'regex': True
    },
    {
        'label': 'Description',
        'property': 'description',
        'regex': True
    },
    {
        'label': 'Category',
        'property': 'category'
    },
    {
        'label': 'On Hand',
        'property': 'stock_on_hand'
    },
    {
        'label': 'Last Transaction',
        'property': 'last_transaction'
    },
    {
        'label': '',
        'property': 'default_unit',
        'hidden': True
    },
]

STOCK_CARD_DATA_COLUMNS = [
    {
        'label': 'Date',
        'property': 'date_created',
        'regex': True
    },
    {
        'label': 'Type',
        'property': 'type',
        'regex': True
    },
    {
        'label': 'Reference',
        'property': 'reference'
    },
    {
        'label': 'Location',
        'property': 'location'
    },
    {
        'label': 'In',
        'property': 'cumulative_quantity'
    },
    {
        'label': 'Out',
        'property': 'cumulative_quantity'
    },
    {
        'label': 'Balance',
        'property': 'aggregate_quantity'
    },
    {
        'label': 'Price per Unit',
        'property': 'price_per_unit'
    },
    {
        'label': 'Conducted By',
        'property': 'created_by'
    },
    {
        'label': 'Expiry Date',
        'property': 'expiry_date'
    },
    {
        'label': 'Manufacturing Date',
        'property': 'manufacturing_date'
    },
    {
        'label': '',
        'property': 'raw_material',
        'hidden': True
    },
    {
        'label': '',
        'property': 'delivery_receipt',
        'hidden': True
    },
    {
        'label': '',
        'property': 'quality_control_no',
        'hidden': True
    },
    {
        'label': '',
        'property': 'manufacturing_order',
        'hidden': True
    },
    {
        'label': '',
        'property': 'used_quality_control_nos',
        'hidden': True
    }
]


@bp.before_request
def before_request():
    request.mod = MODULE_NAME
    if not session['user'].get('access_levels', {}).get('permissions', {}).get(request.mod, {}).get('read', False):
        if request.referrer:
            return redirect(request.referrer)
        return redirect(url_for('default.default'))


@bp.route('/', methods=['GET'])
@bp.csrf_protect
def index():
    return index_page()


@bp.route('/<objectid:_id>', methods=['GET'])
@bp.csrf_protect
def edit(_id):
    return index_page(_id)


def index_page(_id=None):
    record = None
    if _id:
        record = RawMaterial.grab(_id)
        record['name'] = record['item_code']
    crud = {
        'home': url_for('.index'),
        'create': url_for('.add'),
        'read': {
            'many': {
                'url': url_for('.load'),
                'columns': DATA_COLUMNS
            },
            'one': {
                'url': url_for('.details', _id='placeholder')
            }
        },
        'update_record': url_for('.save'),
        'delete': {
            'many': {
                'url': url_for('.delete_many')
            },
            'one': {
                'url': url_for('.delete', _id='placeholder')
            }
        }
    }
    raw_material_categories = app.cache.lrange('raw_material_category', 0, -1)
    packaging_material_categories = app.cache.lrange('packaging_material_category', 0, -1)
    return render_template('admin/raw_materials/index.html', crud=crud, record=record, categories=raw_material_categories+packaging_material_categories, units=app.cache.lrange('raw_material_unit', 0, -1))


@bp.route('/add', methods=['POST'])
def add():
    try:
        values = request.form.to_dict()
        values['created_by'] = session['user']['_id']
        RawMaterial.create(**values)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/load', methods=['POST'])
def load():
    try:
        return jsonify(RawMaterial.datatable({**request.form.to_dict(), **{'columns': DATA_COLUMNS}}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/<objectid:_id>/details', methods=['GET'])
def details(_id):
    try:
        record = RawMaterial.grab(_id)
        if not isinstance(record, RawMaterial):
            raise
        from models.user import User
        user = User.grab(record['created_by'].id)
        record['created_by'] = user.human_readable() if user else ''
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    raw_material_categories = app.cache.lrange('raw_material_category', 0, -1)
    packaging_material_categories = app.cache.lrange('packaging_material_category', 0, -1)
    return render_template('admin/raw_materials/details.html', record=record, categories=raw_material_categories+packaging_material_categories, units=app.cache.lrange('raw_material_unit', 0, -1))


@bp.route('/<objectid:_id>/stock_card', methods=['GET'])
def stock_card(_id):
    try:
        record = RawMaterial.grab(_id)
        if not isinstance(record, RawMaterial):
            raise
        from models.inventory_transaction import InventoryTransaction
        record['stock_on_hand'] = InventoryTransaction.get_stock_on_hand(record)
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    redirect_from = request.args.get('from', 'records')
    return render_template('admin/raw_materials/stock_card.html', record=record, STOCK_CARD_DATA_COLUMNS=STOCK_CARD_DATA_COLUMNS, redirect_from=redirect_from)


@bp.route('/<objectid:_id>/stock_card', methods=['POST'])
def load_stock_card(_id):
    try:
        record = RawMaterial.grab(_id)
        if not isinstance(record, RawMaterial):
            raise
        from models.inventory_transaction import InventoryTransaction
        return jsonify(InventoryTransaction.datatable({**request.form.to_dict(), **{
                'columns': STOCK_CARD_DATA_COLUMNS,
                'required_filter': {
                    'raw_material.$id': record._id
                }
            }}))
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e, 'data': [], 'draw': request.form.get('draw'), 'recordsTotal': 0, 'recordsFiltered': 0})


@bp.route('/<objectid:_id>/issue_tag', methods=['GET'])
def issue_tag(_id):
    try:
        record = RawMaterial.grab(_id)
        if not isinstance(record, RawMaterial):
            raise
        from models.inventory_transaction import InventoryTransaction
        import arrow
        record['stock_on_hand'] = InventoryTransaction.get_stock_on_hand(record)
        record['last_transaction'] = {
            'spiel': 'N/A',
        }
        record['last_transaction']['record'] = InventoryTransaction.get_last_transaction(record)
        if record['last_transaction']['record']:
            record['last_transaction']['spiel'] = record['last_transaction']['record'].human_readable(record)
    except Exception as e:
        app.logger.exception(e)
        return render_template('common/pages/crud.record_not_found.html')
    redirect_from = request.args.get('from')
    return render_template('admin/raw_materials/issue_tag.html', record=record, redirect_from=redirect_from)


@bp.route('/<objectid:_id>/create_issue_tag', methods=['POST'])
def create_issue_tag(_id):
    try:
        record = RawMaterial.grab(_id)
        if not isinstance(record, RawMaterial):
            raise
        from models.issue_tag import IssueTag
        kwargs = request.form.to_dict()
        kwargs['created_by'] = session['user']['_id']
        IssueTag.create(**kwargs)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/save', methods=['POST'])
def save():
    try:
        record = RawMaterial.grab(request.form.get('_id'))
        if not isinstance(record, RawMaterial):
            raise
        kwargs = request.form.to_dict()
        kwargs.pop('_id')
        record.update(**kwargs)
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/<objectid:_id>/delete', methods=['GET'])
def delete(_id):
    try:
        record = RawMaterial.grab(_id)
        if not isinstance(record, RawMaterial):
            raise
        record.delete()
    except Exception as e:
        app.logger.exception(e)
        return jsonify({'error': e})
    return jsonify({'success': True})


@bp.route('/delete', methods=['GET'])
def delete_many():
    pass
