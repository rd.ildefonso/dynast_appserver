from flask import Blueprint, render_template, redirect, url_for, session, request, abort, flash, make_response, current_app as app
MODULE_NAME = __name__.split('.')[-1]
bp = Blueprint(MODULE_NAME, __name__)


@bp.route('/', methods=['GET'])
def default():
    if 'user' in session:
        return redirect(url_for('dashboard.index'))
    return redirect(url_for('login.form'))


@bp.route('/logout', methods=['GET'])
def logout():
    session.clear()
    flash('You have been logged-out')
    return redirect(url_for('login.form'))
