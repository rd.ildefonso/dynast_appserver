.. _admin:

Admin Guide
==================
This guide will get you started with setting up the platform.
Some of the roles exclusive to an admin include provisioning of users,
enforcing Access Control Policies, and setting up static references
such as raw material categories, and units.

Getting Started
--------------------
Admin modules can be accessed if you have an administrator account.
Input provided credentials to obtain access.

.. figure:: /_static/img/login.png
   :alt: Login Page
   :align: center

   *Image 1: Dynast Flow's Login Page*

Admin Modules
--------------------
Once successfully logged in, the administrator can access the following modules:
 * Users Management
 * Data Setup

On top of these, the administrator will have **full access** to any other modules.
