### Prerequisites
* Install required python modules
    - pip install -r requirements.txt

* Install MongoDB


### Execute
* Simple/Development
    gunicorn -b 0.0.0.0:2020 -k gevent -w 1 wsgi:app
    waitress-serve --listen=0.0.0.0:2020 wsgi:app

    docker run -it --network net-dynast --publish 2020:2020 registry.gitlab.com/rd.ildefonso/dynast_appserver/flow:latest /bin/bash
* Production
    docker pull registry.gitlab.com/rd.ildefonso/dynast_appserver/flow:latest

    docker service create \
    --name dynast-flow \
    --sysctl net.core.somaxconn=2048 \
    --network net-dynast \
    --publish 80:2020 \
    --mount type=bind,source=/home/ubuntu/data/settings.yaml,target=/srv/app/settings.yaml \
    --replicas=1 \
    registry.gitlab.com/rd.ildefonso/dynast_appserver/flow:latest

    docker service scale dynast-flow=n

    docker service logs dynast-flow -f --since 5m
