import csv
import logging
import arrow
from models.user import SuperAdmin

log = logging.getLogger('dynast-db')
log.setLevel(20)
log.info('dynast-db v0.1')

sa = SuperAdmin.check()
start = arrow.utcnow().datetime

def reset_db():
	log.info('executing reset_db')
	from models.client import Client
	from models.data_setup import DataSetup
	from models.delivery_receipt import DeliveryReceipt
	from models.finished_good import FinishedGood
	from models.forecast import Forecast
	from models.formula import Formula
	from models.inventory_transaction import InventoryTransaction
	from models.issue_tag import IssueTag
	from models.location import Location
	from models.manufacturing_order import ManufacturingOrder
	from models.raw_material import RawMaterial
	from models.receiving_report import ReceivingReport
	Client.drop()
	DataSetup.drop()
	DeliveryReceipt.drop()
	FinishedGood.drop()
	Forecast.drop()
	Formula.drop()
	InventoryTransaction.drop()
	IssueTag.drop()
	Location.drop()
	ManufacturingOrder.drop()
	RawMaterial.drop()
	ReceivingReport.drop()


def populate_data_setup():
	log.info('executing populate_data_setup')
	from models.data_setup import RawMaterialCategory, PackagingMaterialCategory
	try:
		if RawMaterialCategory.find_one({'value': 'raw material'}):
			raise ValueError(f'rm category already in records')
		RawMaterialCategory.create(value='raw material')
		if PackagingMaterialCategory.find_one({'value': 'packaging material'}):
			raise ValueError(f'pm category already in records')
		PackagingMaterialCategory.create(value='packaging material')
	except ValueError as ve:
		log.debug(ve)
	except Exception as ex:
		log.error(ex)


def populate_clients(filename):
	log.info('executing populate_clients')
	from models.client import Client
	total = 0
	valid = 0
	errors = []
	with open(filename, encoding='latin-1') as f:
		reader = csv.reader(f)
		for index, row in enumerate(reader):
			if index < 2:
				continue
			total = total + 1
			try:
				if Client.find_one({'name': row[0]}):
					raise ValueError('already in records')
				row = list([r.strip() for r in row if r != None])
				name = row[0].title()
				address = row[1]
				primary_contact_number = row[3] if len(row[3]) > 3 else ''
				secondary_contact_number = row[4] if len(row[4]) > 3 else ''
				business_type = row[5].lower() if row[5] != '' else 'trader'
				tin_number = row[6]
				contact_person = row[7]
				data = {
					'name': name,
					'address': address,
					'primary_contact_number': primary_contact_number,
					'secondary_contact_number': secondary_contact_number,
					'business_type': business_type,
					'tin_number': tin_number,
					'contact_person': contact_person
				}
				data = {k: v for k, v in data.items() if v != ''}
				data['created_by'] = sa
				Client.create(**data)
				valid = valid + 1
			except ValueError as ve:
				errors.append(f'{index} - {row[0]} - {ve}')
			except Exception as ex:
				errors.append(f'{index} - {row[0]} - {ex}')
	errors.sort()
	return total, valid, errors


def populate_materials(filename):
	log.info('executing populate_materials')
	from models.raw_material import RawMaterial
	total = 0
	valid = 0
	errors = []
	with open(filename, encoding='latin-1') as f:
		reader = csv.reader(f)
		for index, row in enumerate(reader):
			if index < 2:
				continue
			total = total + 1
			try:
				if RawMaterial.find_one({'item_code': row[0]}):
					raise ValueError('already in records')
				row = list([r.strip() for r in row if r != None])
				default_unit = row[5].lower()
				default_unit = default_unit[:2]
				item_code = row[0]
				description = row[1].title()
				category = 'raw material' if default_unit in ['kg', 'li'] else 'packaging material'
				data = {
					'item_code': item_code,
					'description': description,
					'category': category,
					'default_unit': default_unit
				}
				data = {k: v for k, v in data.items() if v != ''}
				data['created_by'] = sa
				RawMaterial.create(**data)
				valid = valid + 1
			except ValueError as ve:
				errors.append(f'{index} - {row[0]} - {ve}')
			except Exception as ex:
				errors.append(f'{index} - {row[0]} - {ex}')
	errors.sort()
	return total, valid, errors


def populate_formulas(filename):
	log.info('executing populate_formulas')
	from models.client import Client
	from models.formula import Formula
	from models.raw_material import RawMaterial
	from time import strptime
	total = 0
	valid = 0
	client_dne = []
	errors = []
	with open(filename, encoding='latin-1') as f:
		reader = csv.reader(f)
		for index, row in enumerate(reader):
			if index < 2:
				continue
			total = total + 1
			try:
				# DEBUG
				# if index > 2:
				# 	continue
				row = list([r.strip() for r in row if isinstance(r, str)])
				formula_no = row[5]
				client_name = row[1].title()
				product_code = row[2]
				description = row[3]
				variant = row[4]
				date_original_issue = row[6]
				revision_no = row[8]
				revision_reason = row[9] if len(row[9]) > 3 else ''
				standard_packing_per_piece = row[10]
				default_unit = row[11].lower()
				default_unit = default_unit[:2]
				standard_specific_gravity = row[12]

				client = Client.find_one({'name': client_name.title()})
				if not isinstance(client, Client):
					raise ValueError(f'client {row[1].title()} dne')
				if len(formula_no) < 3:
					from common.utils.generators import generate_alphanum
					formula_no = generate_alphanum(8).upper()
				if Formula.find_one({'product_code': product_code, 'formula_no': formula_no}):
					raise ValueError(f'already in records')
				# Start Typecasting
				if '-' in date_original_issue:
					date_original_issue = date_original_issue.split('-')
					try:
						date_original_issue[1] = strptime(date_original_issue[1], '%b').tm_mon
						date_original_issue[1] = '0' + str(date_original_issue[1])
						date_original_issue[1] = date_original_issue[1][-2:]
						date_original_issue = f'{date_original_issue[1]}/{date_original_issue[0]}/20{date_original_issue[2]}'
					except:
						date_original_issue = '01/01/2003'
				else:
					date_original_issue = '01/01/2003'
				try:
					revision_no = int(revision_no)
				except:
					revision_no = 1
				try:
					standard_packing_per_piece = int(standard_packing_per_piece)
				except:
					standard_packing_per_piece = 1.00
				try:
					standard_specific_gravity = int(standard_specific_gravity)
				except:
					standard_specific_gravity = 1.00
				# End Typecasting
				data = {
					'product_code': product_code,
					'description': description,
					'variant': variant,
					'formula_no': formula_no,
					'date_original_issue': date_original_issue,
					'revision_no': revision_no,
					'revision_reason': revision_reason,
					'standard_packing_per_piece': standard_packing_per_piece,
					'default_unit': default_unit,
					'standard_specific_gravity': standard_specific_gravity,
					'valid': False,
					'raw_materials': [],
					'packaging_materials': []
				}

				# Parse Raw Materials
				rm_total = 0
				for n in range(30):
					rm_item_code_base = 13
					rm_description_base = 43
					rm_percentage_base = 103
					try:
						item_code = row[rm_item_code_base+n].strip().upper()
						description = row[rm_description_base+n].strip()
						quantity = row[rm_percentage_base+n]
						try:
							quantity = float(row[rm_percentage_base+n]) * 100
						except:
							quantity = float(0)
						rm_total += quantity

						if item_code == '0':
							continue

						material = RawMaterial.find_one({'item_code': item_code})
						if not isinstance(material, RawMaterial):
							material = RawMaterial.find_one({'description': description})
							if not isinstance(material, RawMaterial):
								if len(item_code) < 3:
									from common.utils.generators import generate_alphanum
									item_code = generate_alphanum(8).upper()
								material = RawMaterial.create(
									item_code=item_code,
									description=description,
									category='raw material',
									default_unit='kg',
									created_by=sa
								)
						data['raw_materials'].append({
							'index': n,
							'item_code': str(material._id),
							'quantity': quantity
						})
					except Exception as ex:
						raise ValueError('error parsing raw materials')
				# End Parse Raw Materials

				# Parse Packaging Materials
				pm_total = 0
				for n in range(15):
					pm_item_code_base = 163
					pm_description_base = 178
					pm_quantity_base = 208
					try:
						item_code = row[pm_item_code_base+n].strip().upper()
						description = row[pm_description_base+n].strip()
						quantity = row[pm_quantity_base+n]
						try:
							quantity = int(row[pm_quantity_base+n])
						except:
							quantity = 0
						pm_total += quantity

						if item_code == '0':
							continue

						material = RawMaterial.find_one({'item_code': item_code})
						if not isinstance(material, RawMaterial):
							material = RawMaterial.find_one({'description': description})
							if not isinstance(material, RawMaterial):
								if len(item_code) < 3:
									from common.utils.generators import generate_alphanum
									item_code = generate_alphanum(8).upper()
								material = RawMaterial.create(
									item_code=item_code,
									description=description,
									category='packaging material',
									default_unit='pc',
									created_by=sa
								)
						data['packaging_materials'].append({
							'index': n,
							'item_code': str(material._id),
							'quantity': quantity
						})
					except Exception as ex:
						raise ValueError('error parsing packaging materials')
				# End Parse Packaging Materials

				if round(rm_total, 4) >= 100 and pm_total >= 1:
					data['valid'] = True

				data = {k: v for k, v in data.items() if v != ''}
				data['client'] = client
				data['created_by'] = sa
				data['last_modified_by'] = sa
				Formula.create(**data)
				valid = valid + 1
			except ValueError as ve:
				errors.append(f'{index} - {formula_no} - {ve}')
			except Exception as ex:
				errors.append(f'{index} - {formula_no} - {ex}')
	errors.sort()
	return total, valid, errors


def populate_locations():
	log.info('executing populate_locations')
	from models.location import Location


def populate_via_issue_tag(target):
	log.info('executing populate_via_issue_tag')
	from models.raw_material import RawMaterial
	from models.issue_tag import IssueTag
	for rm in RawMaterial.find():
		IssueTag.create(
	        created_by=sa,
	        raw_material=rm,
	        target_stock_level=target,
	        remarks='override via script'
	    )


reset_db()
populate_data_setup()
clients_total, clients_valid, clients_errors = populate_clients('database/clients.csv')
materials_total, materials_valid, materials_errors = populate_materials('database/materials.csv')
formulas_total, formulas_valid, formulas_errors = populate_formulas('database/formulas.csv')
populate_via_issue_tag(10000)

with open('database_summary.txt', 'w+') as f:
	f.write('dynast-db migrate v0.1' + '\n')
	f.write(f'clients - {clients_valid}/{clients_total} ({clients_valid/clients_total*100}%)' + '\n')
	f.write(f'materials - {materials_valid}/{materials_total} ({materials_valid/materials_total*100})' + '\n')
	f.write(f'formulas - {formulas_valid}/{formulas_total} ({formulas_valid/formulas_total*100})' + '\n')
	f.write('\n')
	f.write(f'--clients errors--\n')
	f.write('\n'.join(clients_errors))
	f.write('\n')
	f.write(f'--materials errors--\n')
	f.write('\n'.join(materials_errors))
	f.write('\n')
	f.write(f'--formulas errors--\n')
	f.write('\n'.join(formulas_errors))
	f.write('\n\n\n')
	delta = arrow.utcnow().datetime - start
	log.info(f'took {delta}')
	f.write(f'took {delta}')
