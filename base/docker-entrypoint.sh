#!/bin/bash

set -e

redis-server --daemonize yes &

while ! nc -z -v -w5 localhost 6379;
  do
    echo 'loading cache engine...';
    sleep 5s;
  done
exec "$@"
