from setuptools import setup, find_packages

setup(
    name='dynast',
    version='0.0',
    description='Dynast Cosmetics & Household Manufacturing Inc. library',
    url='http://www.dynastchi.com',
    author='Rudolf Ildefonso',
    author_email='rd.ildefonso@yahoo.com',
    python_requires='~=3.6.5',
    packages=find_packages(),
    install_requires=[req.strip() for req in open('requirements.txt')],
    zip_safe=False,
    include_package_data=True
)
