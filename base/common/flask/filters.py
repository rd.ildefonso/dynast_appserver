from settings import DATETIME_FORMAT, DEFAULT_TIMEZONE
import inflection
import arrow


def normalize(value):
    return inflection.humanize(value)


def singularize(value):
    return inflection.singularize(value)


def humandate(value, date_only=False):
    if date_only:
        return arrow.get(value).to(DEFAULT_TIMEZONE).datetime.strftime('%m/%d/%Y')
    return arrow.get(value).to(DEFAULT_TIMEZONE).datetime.strftime(DATETIME_FORMAT)


class FilterFactory(object):

    def __init__(self):
        self.filters = {}

    def add_filter(self, filter_name, filter_function):
        self.filters[filter_name] = filter_function

    def get_filters(self):
        return self.filters


filter_factory = FilterFactory()
filter_factory.add_filter('normalize', normalize)
filter_factory.add_filter('singularize', singularize)
filter_factory.add_filter('humandate', humandate)
