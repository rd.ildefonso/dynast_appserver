from werkzeug.routing import BaseConverter, ValidationError


class ObjectIDConverter(BaseConverter):
    def to_python(self, value):
        from common.mogo.model import ObjectId
        try:
            return ObjectId(value)
        except:
            raise ValidationError()

    def to_url(self, value):
        return str(value)


class ConverterFactory(object):
    def __init__(self):
        self.converters = {}

    def add_converter(self, url_map, converter_class):
        self.converters[url_map] = converter_class

    def get_converters(self):
        return self.converters


converter_factory = ConverterFactory()
converter_factory.add_converter('objectid', ObjectIDConverter)
