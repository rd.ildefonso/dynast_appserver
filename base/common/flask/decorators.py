from functools import wraps
from flask import request, session, abort
from common.utils.generators import generate_alphanum


def csrf_protect(blueprint):
    def wrapped(f):
        @wraps(f)
        def inner_wrapped(*args, **kwargs):
            if request.method == 'GET':
                session['csrf_key'] = generate_alphanum(8)
                session['csrf_token'] = generate_alphanum(16)
            elif request.method == 'POST':
                if not request.form:
                    blueprint.logger.error('CSRF Error - No Data')
                    return abort(412)
                if session.get('csrf_key', 'none_existing') not in request.form.keys():
                    blueprint.logger.error('CSRF Error - Missing Key')
                    return abort(412)
                session['csrf_key'] = generate_alphanum(8)
                session['csrf_token'] = generate_alphanum(16)
            return f(*args, **kwargs)
        return inner_wrapped
    return wrapped


class DecoratorFactory(object):
    def __init__(self):
        self.decorators = {}

    def add_decorator(self, decorator_name, decorator_function):
        self.decorators[decorator_name] = decorator_function

    def integrate_to_blueprint(self, blueprint):
        for key, value in self.decorators.items():
            setattr(blueprint, key, value(blueprint))


decorator_factory = DecoratorFactory()
decorator_factory.add_decorator('csrf_protect', csrf_protect)
