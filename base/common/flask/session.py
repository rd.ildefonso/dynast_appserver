import uuid
from common.utils.jsonify import jsonify, unjsonify
from flask.sessions import SessionMixin, SessionInterface
from redis import ConnectionPool, Redis
from settings import APPSERVER_NAME
from werkzeug.datastructures import CallbackDict


class RedisSession(CallbackDict, SessionMixin):
    def __init__(self, identifier, sid, connection, initial=None):
        CallbackDict.__init__(self, initial)
        self.identifier = identifier
        self.sid = sid
        self.modified = False
        self.connection = connection

    def clear(self):
        self.connection.delete(f'{self.identifier}:{self.sid}')
        super(RedisSession, self).clear()

    def count(self):
        pass


class RedisSessionInterface(SessionInterface):
    def __init__(self, config):
        if not isinstance(config, dict):
            raise Exception('Invalid session configuration')
        for key in ['identifier', 'expiry', 'max_unique', 'max_concurrent']:
            if key not in config:
                raise Exception('Missing session configuration')
        self.identifier = config.get('identifier')
        self.expiry = config.get('expiry', 3600)
        self.max_unique = config.get('max_unique')
        self.max_concurrent = config.get('max_concurrent')

    def open_session(self, app, request):
        sid = request.cookies.get(app.session_cookie_name)
        connection = Redis(connection_pool=app._redis_pool)
        if sid:
            key = f'{self.identifier}:{sid}'
            if connection.exists(key):
                return RedisSession(identifier=self.identifier, sid=sid, connection=connection, initial=unjsonify(connection.get(key)))
        sid = str(uuid.uuid4())
        return RedisSession(identifier=self.identifier, sid=sid, connection=connection)

    def save_session(self, app, session, response):
        # if not session:
        #     response.delete_cookie(app.session_cookie_name)
        #     return
        connection = Redis(connection_pool=app._redis_pool)
        key = f'{self.identifier}:{session.sid}'
        if connection.exists(key):
            # update only
            connection.set(key, jsonify(session, as_string=True))
        else:
            # create and set expiry
            connection.set(key, jsonify(session, as_string=True))
            connection.expire(key, self.expiry)
        # set cookie
        response.set_cookie(app.session_cookie_name, session.sid, expires=self.get_expiration_time(app, session), httponly=True)
