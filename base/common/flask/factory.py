import logging
from flask import Flask, Blueprint, request, session, abort, render_template
from flask_assets import Environment
from pkgutil import iter_modules
from importlib import import_module
from socket import gethostname
from redis import ConnectionPool, Redis
from common.utils.hash import hash
from common.flask.filters import filter_factory
from common.flask.converters import converter_factory
from common.flask.session import RedisSessionInterface
from settings import APPSERVER_NAME, ENABLED_APPS, DEBUG, LOG_FORMAT, DATETIME_FORMAT, DEFAULT_TIMEZONE, DEVELOPMENT_MODE, REDIS, SESSION

logging.basicConfig(format=LOG_FORMAT, level=logging.DEBUG if DEBUG else logging.INFO)
log = logging.getLogger('app.factory')
TARGET_ERRORS = [403, 404, 412, 500]


class AppFactory(object):

    def __init__(self, appserver_name, enabled_apps=None):
        self.appserver_name = appserver_name
        self.hostname = gethostname()
        self.enabled_apps = enabled_apps if isinstance(enabled_apps, list) else 'ALL'
        self.apps = {}
        self.app_count = 0
        self.primary_app = None

    def add_app(self, app_name, package_path, **kwargs):
        app_alias = app_name.split('.')[-1]
        log.debug(f'Creating - {app_alias}')

        def errorhandler(Exception):
            request.mod = 'error'
            try:
                Exception.code
            except:
                Exception.code = 500
            return render_template('common/pages/error.html', error=Exception)

        def register_blueprints(app, app_name, package_path):
            for _, module_name, __ in iter_modules(package_path):
                module = import_module('%s.%s' % (app_name, module_name))
                module_attributes = dir(module)
                if 'ignore_autoload' in module_attributes:
                    continue
                for item in module_attributes:
                    attribute = getattr(module, item)
                    if isinstance(attribute, Blueprint):
                        blueprint = attribute
                        blueprint.logger = logging.getLogger('.'.join([self.appserver_name, app_name.split('.')[-1], module_name]))
                        app.register_blueprint(attribute)

        def before_request():
            request.appserver_name = self.appserver_name

        app = None
        try:
            app = Flask(app_alias, **kwargs)
            app.debug = DEBUG

            app.logger = logging.getLogger('.'.join([self.appserver_name, app_alias]))
            app.logger.setLevel(logging.DEBUG if DEBUG else logging.INFO)

            app.jinja_env.filters = {**app.jinja_env.filters, **filter_factory.get_filters()}
            app.jinja_env.trim_blocks = True
            app.jinja_env.lstrip_blocks = True
            app.url_map.converters = {**app.url_map.converters, **converter_factory.get_converters()}
            app.assets = Environment(app)

            app.default_timezone = DEFAULT_TIMEZONE
            app.datetime_format = DATETIME_FORMAT

            app.config['SESSION_COOKIE_NAME'] = app_alias
            app.secret_key = hash.generate(app_name)
            app.permanent_session_lifetime = SESSION.get('expiry', 3600)
            app._redis_pool = ConnectionPool(**REDIS)
            app.session_interface = RedisSessionInterface(config=SESSION)
            app.cache = Redis(connection_pool=app._redis_pool)

            for error_code in TARGET_ERRORS:
                app.register_error_handler(error_code, errorhandler)
            app.before_request(before_request)

            register_blueprints(app, app_name, package_path)
            # NOTE: App creation finished

            self.apps[app_alias] = app
            self.app_count += 1
            if not self.primary_app:
                self.primary_app = app_alias
            app.logger.debug('Passed')
        except Exception as e:
            log.exception(f'Error: {e}')
        return app

    def bundle(self):
        if DEVELOPMENT_MODE:
            log.debug('Running on Development Mode')
        log.debug(f'Consolidating Apps - {"/".join(self.apps.keys())}')
        if self.enabled_apps != 'ALL':
            self.apps = {k: v for k, v in self.apps.items() if k in self.enabled_apps}
        if len(self.apps.keys()) == 1:
            key = list(self.apps.keys())[0]
            log.info(f'Created Single App - {key}')
            return self.apps[key]
        else:
            log.info(f'Created Multi-tenant App - {"/".join(self.apps.keys())}')
            from werkzeug.middleware.dispatcher import DispatcherMiddleware
            return DispatcherMiddleware(self.apps.pop(self.primary_app), dict({f'/{k}': v for k, v in self.apps.items()}))
        return None

app_factory = AppFactory(APPSERVER_NAME, enabled_apps=ENABLED_APPS)
