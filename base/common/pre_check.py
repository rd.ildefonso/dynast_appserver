import sys
import platform
import importlib


class PreCheck(object):
    def __init__(self, required_python_version=None, required_os=None):
        info = platform.uname()
        self.current_os = info[0]
        self.current_version = tuple(int(i) for i in platform.python_version_tuple())
        self.current_release = '%s v%s %s' % (info[0], info[2], info[4])
        if required_python_version != None:
            try:
                self.required_version = tuple((int(i) for i in required_python_version.split('.')[:]))
            except:
                sys.stderr.write('Can\'t parse required version - %s\n' % (self.required_version))
                sys.exit(1)
        else:
            self.required_version = self.current_version
        self.required_os = required_os if required_os else self.current_os


    def __iter__():
        return {k: v for k, v in {}.items()}


    def enforce(self):
        if self.required_os != self.current_os:
            sys.stderr.write('Wrong OS detected\n')
            sys.exit(1)
        if self.required_version > self.current_version:
            sys.stderr.write('Python v%s is required\n' % ('.'.join([str(i) for i in self.required_version])))
            sys.exit(1)
        importlib.reload(sys)
