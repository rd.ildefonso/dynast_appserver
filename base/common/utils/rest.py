from common.mogo.model import ObjectId


def parse_args(rargs):
    filters = {k: v if v.lower() not in ['true', 'false'] else True if v.lower() == 'true' else False for k, v in rargs.items() if k != 'fields'}
    filters = {k: v if k != '_id' else ObjectId(v) for k, v in filters.items()}
    filters = filters if filters != {} else None
    fields = None
    if 'fields' in rargs:
        fields = {i: True for i in rargs.get('fields').split(',')}
    else:
        fields = ['_id']
    return filters, fields
