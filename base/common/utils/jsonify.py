try:
    import ujson as json
except:
    import json
import bson
import datetime
import arrow
import werkzeug
import common.mogo.cursor
import common.mogo.model
from settings import DATETIME_FORMAT, DEFAULT_TIMEZONE


class MongoEncoder(json.JSONEncoder):
    def default(self, object):
        if isinstance(object, datetime.datetime):
            return object.astimezone(DEFAULT_TIMEZONE).strftime(DATETIME_FORMAT)
        if isinstance(object, arrow.Arrow):
            return object.datetime.astimezone(DEFAULT_TIMEZONE).strftime(DATETIME_FORMAT)
        if isinstance(object, common.mogo.model.ObjectId):
            return str(object)
        if isinstance(object, Exception):
            return str(object)
        if isinstance(object, common.mogo.cursor.Cursor):
            return list(object)
        if isinstance(object, common.mogo.model.DBRef):
            return str(object.id)
        return json.JSONEncoder.default(self, object)


def jsonify(*args, **kwargs):
    as_string = kwargs.pop('as_string', False)
    json_string = json.dumps(dict(*args, **kwargs), cls=MongoEncoder)
    if as_string:
        return json_string
    else:
        return werkzeug.Response(json_string, mimetype='application/json')


def unjsonify(string):
    return json.loads(string)
