import logging
import jinja2
import os

logging.basicConfig(format='%(asctime)s - %(name)s - %(message)s', level=logging.DEBUG)
log = logging.getLogger('txt.reader')


class TxtReader(object):

    def __init__(self, file_path, _vars={}):
        logo = ''
        if os.path.isfile(file_path):
            with open(file_path, 'r') as f:
                try:
                    logo = ''.join(f.readlines())
                except Exception as e:
                    log.exception(f'Txt Error: {e}')
        self.logo = jinja2.Template(logo)
        self.vars = vars(_vars)

    def render(self):
        print(self.logo.render(self.vars))
