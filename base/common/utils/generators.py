import random


def generate_alphanum(length):
    chars = 'atuvK12bcdewPQRSTUVfFGHIJopq7xyzAhijklmnWXr456YZ890s3BCDELMNOg'
    result = [random.choice(chars) for k in range(0, length)]
    return ''.join(result)


def generate_random_password():
    return generate_alphanum(8)
