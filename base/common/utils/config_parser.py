import logging
import sys
import os
import yaml

logging.basicConfig(format='%(asctime)s - %(name)s - %(message)s', level=logging.DEBUG)
log = logging.getLogger('config.parser')


class Config(object):
    defaults = {}
    overrides = {}

    def get(self):
        return {**self.defaults, **self.overrides}

    def validate(self, exit=False):
        if self.defaults == {}:
            log.error('Empty Defaults')
            if exit:
                sys.exit(1)
            return None
        return self


class YAML(Config):
    
    def __init__(self, *args):
        def read_yaml(file_path):
            if os.path.isfile(file_path):
                with open(file_path, 'r') as f:
                    try:
                        result = yaml.load(f)
                        return result
                    except Exception as e:
                        log.exception(f'YAML Error: {e}')
            else:
                log.error(f'File not found: {file_path}')
            return {}
        for i, arg in enumerate(args):
            if i == 0:
                self.defaults = read_yaml(args[i])
                continue
            else:
                overrides = {k: v for k, v in read_yaml(args[i]).items() if k in self.defaults}
                self.overrides = {**self.overrides, **overrides}
