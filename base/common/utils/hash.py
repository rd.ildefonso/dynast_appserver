from hashlib import sha256
try:
    from pbkdf2 import crypt
    PBKDF2_OK = True
except Exception as ex:
    PBKDF2_OK = False
from common.utils.generators import generate_alphanum
from settings import APPSERVER_NAME


class Hash(object):

    def __init__(self, pbkdf2=True, salt_length=24, cost_factor=12000):
        self.pbkdf2 = all([pbkdf2, PBKDF2_OK])
        self.cost_factor = cost_factor
        self.dynamic_salt = generate_alphanum(salt_length)
        self.static_salt = APPSERVER_NAME

    def generate(self, string):
        if self.pbkdf2:
            if isinstance(string, str):
                string = string.encode('utf-8')
            while True:
                try:
                    _hash = crypt(string, self.dynamic_salt, self.cost_factor)
                    break
                except:
                    pass
            return 'pbkdf$%s$%s$%s' % (self.dynamic_salt, self.cost_factor, _hash)
        salt_len = len(self.static_salt)
        string = '%s%s%s' % (self.static_salt[0:int(salt_len/2)], string, self.static_salt[int((salt_len/2)+1):-1])
        return sha256(string.encode('utf-8')).hexdigest()

    def check(self, reference_string, test_string):
        if self.pbkdf2:
            if isinstance(test_string, str):
                test_string = test_string.encode('utf-8')
            try:
                algo, salt, cost_factor, _hash = reference_string.split('$', 3)
            except:
                return False
            if _hash == crypt(test_string, _hash):
                return True
            return False
        salt_len = len(self.static_salt)
        test_string = '%s%s%s' % (self.static_salt[0:int(salt_len/2)], test_string, self.static_salt[int((salt_len/2)+1):-1])
        return sha256(test_string.encode('utf-8')).hexdigest()

hash = Hash()
