""" This is the mogo syntactic sugar library for MongoDB. """

from common.mogo.model import Model, PolyModel
from common.mogo.field import Field, ReferenceField, ConstantField, EnumField
from common.mogo.cursor import ASC, DESC
from common.mogo.connection import connect, session

# Allows flexible (probably dangerous) automatic field creation for
# /really/ schemaless designs.
AUTO_CREATE_FIELDS = False

__all__ = [
    'Model', 'PolyModel', 'Field', 'ReferenceField', "ConstantField",
    "EnumField", 'connect', 'session', 'ASC', 'DESC', "AUTO_CREATE_FIELDS"
]
