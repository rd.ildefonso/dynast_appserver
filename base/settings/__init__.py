from pytz import timezone
from common.utils.config_parser import YAML


settings = YAML('/srv/base/settings/defaults.yaml', 'settings.yaml').validate()
settings = {k.upper(): v if k != 'default_timezone' else timezone(v) for k, v in settings.get().items()}
locals().update(settings)
