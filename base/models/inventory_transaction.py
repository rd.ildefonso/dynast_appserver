import arrow
from settings import DATETIME_FORMAT, DEFAULT_TIMEZONE
from datetime import datetime
from common.mogo import Model, Field, ReferenceField
from models.raw_material import RawMaterial
from models.location import Location
from models.delivery_receipt import DeliveryReceipt
from models.client import Client
from models.issue_tag import IssueTag
from models.manufacturing_order import ManufacturingOrder
from models.user import User


class InventoryTransaction(Model):
    date_created = Field(datetime, required=True)
    created_by = ReferenceField(User, required=True)

    # NOTE: delivery (RR) / issue_tag (IT) / used (MO) / return (MO) / additional (MO)
    type = Field(str, required=True)

    raw_material = ReferenceField(RawMaterial, required=True)
    price_per_unit = Field(float)
    quality_control_no = Field(str)
    location = ReferenceField(Location)
    expiry_date = Field(datetime)
    manufacturing_date = Field(datetime)

    cumulative_quantity = Field(float)
    aggregate_quantity = Field(float)

    used_quantity = Field(float)
    exhausted = Field(bool)

    delivery_receipt = ReferenceField(DeliveryReceipt)
    client_for = ReferenceField(Client)
    client_from = ReferenceField(Client)
    issue_tag = ReferenceField(IssueTag)
    manufacturing_order = ReferenceField(ManufacturingOrder)
    used_quality_control_nos = Field(list)

    @classmethod
    def create(cls, **kwargs):
        if not isinstance(kwargs.get('created_by'), User):
            try:
                kwargs['created_by'] = User.grab(kwargs.get('created_by'))
            except:
                raise Exception('Session has Expired')
        if not isinstance(kwargs['raw_material'], RawMaterial):
            kwargs['raw_material'] = RawMaterial.grab(kwargs['raw_material'])
        if kwargs.get('price_per_unit'):
            kwargs['price_per_unit'] = float(kwargs['price_per_unit'])
        if kwargs['type'] != 'issue_tag':
            if 'cumulative_quantity' in kwargs:
                kwargs['cumulative_quantity'] = float(kwargs['cumulative_quantity'])
            else:
                return
        if kwargs['type'] == 'issue_tag':
            if 'aggregate_quantity' in kwargs:
                kwargs['aggregate_quantity'] = float(kwargs['aggregate_quantity'])
            else:
                return
        if kwargs.get('location'):
            if not isinstance(kwargs['location'], Location):
                kwargs['location'] = Location.grab(kwargs['location'])
        if kwargs.get('expiry_date', '') != '':
            if not isinstance(kwargs['expiry_date'], datetime):
                try:
                    kwargs['expiry_date'] = arrow.get(kwargs['expiry_date']).datetime
                except:
                    kwargs.pop('expiry_date')
        else:
            kwargs.pop('expiry_date', None)
        if kwargs.get('manufacturing_date', '') != '':
            if not isinstance(kwargs['manufacturing_date'], datetime):
                try:
                    kwargs['manufacturing_date'] = arrow.get(kwargs['manufacturing_date']).datetime
                except:
                    kwargs.pop('manufacturing_date')
        else:
            kwargs.pop('manufacturing_date', None)
        if kwargs.get('delivery_receipt'):
            if not isinstance(kwargs['delivery_receipt'], DeliveryReceipt):
                kwargs['delivery_receipt'] = DeliveryReceipt.grab(kwargs['delivery_receipt'])
        if kwargs.get('client_for'):
            if not isinstance(kwargs['client_for'], Client):
                kwargs['client_for'] = Client.grab(kwargs['client_for'])
        if kwargs.get('client_from'):
            if not isinstance(kwargs['client_from'], Client):
                kwargs['client_from'] = Client.grab(kwargs['client_from'])
        if kwargs.get('issue_tag'):
            if not isinstance(kwargs['issue_tag'], IssueTag):
                kwargs['issue_tag'] = IssueTag.grab(kwargs['issue_tag'])
        if kwargs.get('manufacturing_order'):
            if not isinstance(kwargs['manufacturing_order'], ManufacturingOrder):
                kwargs['manufacturing_order'] = ManufacturingOrder.grab(kwargs['manufacturing_order'])
        return super().create(**kwargs)

    @classmethod
    def get_stock_on_hand(cls, raw_material, date_created=None):
        # NOTE: date_created for relative stock on hand
        if not isinstance(raw_material, RawMaterial):
            return -1
        quantity = 0
        filters = {'raw_material.$id': raw_material.id, 'type': {'$ne': 'issue_tag'}}
        if date_created:
            filters = {**filters, **{
                'date_created': {
                    '$lte': date_created
                }
            }}
            last_issue_tag = list(cls.find({'raw_material.$id': raw_material._id, 'type': 'issue_tag', 'date_created': {'$lte': date_created}}).sort([('date_created', -1)]).limit(1))
        else:
            last_issue_tag = list(cls.find({'raw_material.$id': raw_material._id, 'type': 'issue_tag'}).sort([('date_created', -1)]).limit(1))
        if last_issue_tag:
            quantity = last_issue_tag[0].aggregate_quantity
            filters = {**filters, **{
                'date_created': {
                    '$gte': last_issue_tag[0].date_created
                }
            }}
        quantity += sum([it.cumulative_quantity for it in cls.find(filters)])
        return quantity

    def get_relative_stock_on_hand(self):
        return self.__class__.get_stock_on_hand(RawMaterial.grab(self.raw_material.id), date_created=self.date_created)

    @classmethod
    def get_last_transaction(cls, reference):
        last_transaction = None
        if isinstance(reference, RawMaterial):
            _last_transaction = list(cls.find({'raw_material.$id': reference.id}).sort([('date_created', -1)]).limit(1))
            if _last_transaction:
                return _last_transaction[0]
        elif isinstance(reference, Client):
            _last_transaction_for = list(cls.find({'client_for.$id': reference.id, 'type': 'delivery'}).sort([('date_created', -1)]).limit(1))
            _last_transaction_from = list(cls.find({'client_from.$id': reference.id, 'type': 'delivery'}).sort([('date_created', -1)]).limit(1))
            if _last_transaction_for and not _last_transaction_from:
                return _last_transaction_for[0]
            elif _last_transaction_from and not _last_transaction_for:
                return _last_transaction_from[0]
            elif _last_transaction_for and _last_transaction_from:
                if _last_transaction_for[0].date_created > _last_transaction_from[0].date_created:
                    return _last_transaction_for[0]
                else:
                    return _last_transaction_from[0]
        return last_transaction

    def human_readable(self, reference):
        self['date_created'] = arrow.get(self['date_created']).to(DEFAULT_TIMEZONE).datetime.strftime(DATETIME_FORMAT)
        if isinstance(reference, RawMaterial):
            if self.type == 'delivery':
                client_for = Client.grab(self.client_for.id)
                client_from = Client.grab(self.client_from.id)
                return f'{self.cumulative_quantity} {reference.default_unit} delivered by {client_from.name} for {client_for.name} ({self.date_created})'
            elif self.type == 'issue_tag':
                issue_tag = IssueTag.grab(self.issue_tag.id)
                user = User.grab(issue_tag.created_by.id)
                raw_material = RawMaterial.grab(issue_tag.raw_material.id)
                return f'Actual Count {issue_tag.target_stock_level} {raw_material.default_unit} by {user.human_readable()} ({self.date_created})'
            elif self.type == 'used':
                manufacturing_order = ManufacturingOrder.grab(self.manufacturing_order.id)
                from models.formula import Formula
                formula = Formula.grab(manufacturing_order.formula_no.id)
                return f'Used for producing {manufacturing_order.batch_size} {formula.default_unit} of {formula.description} {formula.variant} ({self.date_created})'
            elif self.type == 'return':
                pass
            else:
                return 'Unrecognized Type'
        elif isinstance(reference, Client):
            if self.client_for.id == reference._id:
                raw_material = RawMaterial.grab(self.raw_material.id)
                client_from = Client.grab(self.client_from.id)
                return f'Received {self.cumulative_quantity} {raw_material.default_unit} of {raw_material.description} ({raw_material.item_code}) from {client_from.name} ({self.date_created})'
            elif self.client_from.id == reference._id:
                raw_material = RawMaterial.grab(self.raw_material.id)
                client_for = Client.grab(self.client_for.id)
                return f'Delivered {self.cumulative_quantity} {raw_material.default_unit} of {raw_material.description} ({raw_material.item_code}) to {client_for.name} ({self.date_created})'
        return 'Cannot be determined'

    @classmethod
    def datatable(cls, vars):
        # START: Common
        columns = vars.get('columns', [])
        columns = {item.get('property'): True for item in columns if item.get('property', '') != ''}
        columns['_id'] = True
        required_filter = vars.pop('required_filter', {})
        limit = int(vars.get('length', 0))
        limit = 0 if limit < 0 else limit
        start = int(vars.get('start', 0))
        query_text = vars.get('search[value]', '')
        sort = [None for i in range(len(columns))]
        for k, v in {k: v for k, v in vars.items() if 'order[' in k and '][column]' in k}.items():
            sort_index = int(k.replace('order[', '').replace('][column]', ''))
            col_index = int(v)
            direction = 1 if vars.get(f'order[{sort_index}][dir]') == 'asc' else -1
            sort[sort_index] = (vars.get(f'columns[{col_index}][data]'), direction)
        sort = [s for s in sort if s != None]
        all = cls.find({}, columns)
        filter = {'$or': [{col.get('property'): {'$regex': query_text, '$options': '-i'}} for col in vars.get('columns', []) if col.get('regex') == True and query_text != '']}
        if filter['$or'] == []:
            filter.pop('$or')
        filter = {**required_filter, **filter}
        _data = cls.find(filter, columns)
        data = list(_data.sort(sort).limit(limit).skip(start))
        # END: Common

        for index, item in enumerate(data):
            data[index] = dict(item)
            if 'location' in item:
                data[index]['location'] = Location.grab(item.location.id)['name']
            if item['type'] == 'issue_tag':
                data[index]['reference'] = ''
                data[index]['cumulative_quantity'] = 0
                data[index]['price_per_unit'] = 0
            else:
                data[index]['reference'] = ''
                if item['type'] == 'delivery':
                    delivery_receipt = DeliveryReceipt.grab(item.delivery_receipt.id)
                    data[index]['reference'] = {'link': f'/delivery_receipts/{item.delivery_receipt.id}', 'label': f'DR #{delivery_receipt.receipt_no} - QC #{item.quality_control_no}'}
                elif item['type'] == 'used':
                    manufacturing_order = ManufacturingOrder.grab(item.manufacturing_order.id)
                    data[index]['reference'] = {'link': f'/manufacturing_orders/{item.manufacturing_order.id}', 'label': f'MO #{manufacturing_order.manufacturing_order_no}\nQC#s: {chr(10).join(item.used_quality_control_nos)}'}
                data[index]['aggregate_quantity'] = item.get_relative_stock_on_hand()
            data[index]['date_created'] = arrow.get(item['date_created']).to(DEFAULT_TIMEZONE).datetime.strftime(DATETIME_FORMAT)
            created_by = User.grab(item.created_by.id)
            data[index]['created_by'] = created_by.human_readable() if created_by else ''
            data[index].pop('raw_material', None)
            data[index].pop('delivery_receipt', None)
        return {'data': data, 'recordsTotal': all.count(), 'recordsFiltered': _data.count()}
