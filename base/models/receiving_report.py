import arrow
from settings import DATETIME_FORMAT, DEFAULT_TIMEZONE
from datetime import datetime
from common.mogo import Model, Field, ReferenceField
from models.raw_material import RawMaterial
from models.delivery_receipt import DeliveryReceipt
from models.location import Location
from models.user import User


class ReceivingReport(Model):
    date_created = Field(datetime, required=True)

    raw_material = ReferenceField(RawMaterial, required=True)
    delivery_receipt = ReferenceField(DeliveryReceipt, required=True)
    quantity = Field(float, required=True)
    price_per_unit = Field(float, required=True)
    expiry_date = Field(datetime)
    manufacturing_date = Field(datetime)

    # NOTE: 0 - Pending / 1 - QA OK / 2 - Approved
    status = Field(int, required=True, default=0)

    quality_by = ReferenceField(User)
    date_quality = Field(datetime)
    quality_control_no = Field(str)
    location = ReferenceField(Location)

    approved_by = ReferenceField(User)
    date_approved = Field(datetime)

    @classmethod
    def create(cls, **kwargs):
        kwargs['quantity'] = round(float(kwargs['quantity']), 4)
        kwargs['price_per_unit'] = round(float(kwargs['price_per_unit']), 2)
        if not isinstance(kwargs['raw_material'], RawMaterial):
            kwargs['raw_material'] = RawMaterial.grab(kwargs['raw_material'])
        if not isinstance(kwargs['delivery_receipt'], DeliveryReceipt):
            kwargs['delivery_receipt'] = DeliveryReceipt.grab(kwargs['delivery_receipt'])
        if kwargs.get('expiry_date', '') != '':
            if not isinstance(kwargs['expiry_date'], datetime):
                try:
                    kwargs['expiry_date'] = arrow.get(kwargs['expiry_date']).datetime
                except:
                    kwargs.pop('expiry_date')
        else:
            kwargs.pop('expiry_date', None)
        if kwargs.get('manufacturing_date', '') != '':
            if not isinstance(kwargs['manufacturing_date'], datetime):
                try:
                    kwargs['manufacturing_date'] = arrow.get(kwargs['manufacturing_date']).datetime
                except:
                    kwargs.pop('manufacturing_date')
        else:
            kwargs.pop('manufacturing_date', None)
        return super().create(**kwargs)

    def update(self, **kwargs):
        if kwargs.get('raw_material'):
            if not isinstance(kwargs['raw_material'], RawMaterial):
                kwargs['raw_material'] = RawMaterial.grab(kwargs['raw_material'])
        if kwargs.get('delivery_receipt'):
            if not isinstance(kwargs['delivery_receipt'], DeliveryReceipt):
                kwargs['delivery_receipt'] = DeliveryReceipt.grab(kwargs['delivery_receipt'])
        super().update(**kwargs)

    @classmethod
    def datatable(cls, vars):
        # START: Common
        columns = vars.get('columns', [])
        columns = {item.get('property'): True for item in columns if item.get('property', '') != ''}
        columns['_id'] = True
        required_filter = vars.pop('required_filter', {})
        limit = int(vars.get('length', 0))
        limit = 0 if limit < 0 else limit
        start = int(vars.get('start', 0))
        query_text = vars.get('search[value]', '')
        sort = [None for i in range(len(columns))]
        for k, v in {k: v for k, v in vars.items() if 'order[' in k and '][column]' in k}.items():
            sort_index = int(k.replace('order[', '').replace('][column]', ''))
            col_index = int(v)
            direction = 1 if vars.get(f'order[{sort_index}][dir]') == 'asc' else -1
            sort[sort_index] = (vars.get(f'columns[{col_index}][data]'), direction)
        sort = [s for s in sort if s != None]
        all = cls.find({}, columns)
        filter = {'$or': [{col.get('property'): {'$regex': query_text, '$options': '-i'}} for col in vars.get('columns', []) if col.get('regex') == True and query_text != '']}
        if filter['$or'] == []:
            filter.pop('$or')
        filter = {**required_filter, **filter}
        _data = cls.find(filter, columns)
        data = list(_data.sort(sort).limit(limit).skip(start))
        # END: Common

        for index, item in enumerate(data):
            data[index] = dict(item)
            data[index]['raw_material'] = dict(RawMaterial.grab(item['raw_material'].id))
            data[index]['delivery_receipt'] = dict(DeliveryReceipt.grab(item['delivery_receipt'].id))
            from models.client import Client
            data[index]['client_from'] = dict(Client.grab(data[index]['delivery_receipt']['client_from'].id))
            data[index]['client_for'] = dict(Client.grab(data[index]['delivery_receipt']['client_for'].id))
            data[index]['date_quality'] = None
            if 'date_quality' in item:
                data[index]['date_quality'] = arrow.get(item['date_quality']).to(DEFAULT_TIMEZONE).datetime.strftime(DATETIME_FORMAT)
            data[index]['date_approved'] = None
            if 'date_approved' in item:
                data[index]['date_approved'] = arrow.get(item['date_approved']).to(DEFAULT_TIMEZONE).datetime.strftime(DATETIME_FORMAT)
            if 'quality_by' in item:
                user = User.grab(data[index]['quality_by'].id)
                data[index]['quality_by'] = user.human_readable() if user else ''
            if 'approved_by' in item:
                data[index]['approved_by'] = user.human_readable() if user else ''
            data[index]['date_created'] = arrow.get(item['date_created']).to(DEFAULT_TIMEZONE).datetime.strftime('%Y-%m-%d')
        return {'data': data, 'recordsTotal': all.count(), 'recordsFiltered': _data.count()}

    def change_status(self, user_id, **kwargs):
        try:
            kwargs['status'] = int(kwargs['status'])
        except:
            raise Exception('Validation - Invalid Target Status')

        user = User.grab(user_id)
        date_now = arrow.utcnow().datetime
        date_key = ''
        if kwargs['status'] == 1:
            date_key = 'date_quality'
            if 'quality_control_no' not in kwargs:
                raise Exception('Validation - QC Number is required')
            if 'location' not in kwargs:
                raise Exception('Validation - Location is required')
            kwargs['quality_control_no'] = kwargs['quality_control_no'].upper()
            same_quality_control_no = self.__class__.find_one({'quality_control_no': kwargs['quality_control_no'], '_id': {'$ne': self._id}})
            if same_quality_control_no:
                quality_control_no = kwargs['quality_control_no']
                raise Exception(f'Validation - Record with QC No "{quality_control_no}" Already Exists')
            from models.location import Location
            kwargs['location'] = Location.grab(kwargs['location'])
            kwargs['quality_by'] = user
        elif kwargs['status'] == 2:
            date_key = 'date_approved'
            from models.location import Location
            from models.client import Client
            from models.inventory_transaction import InventoryTransaction

            raw_material = RawMaterial.grab(self.raw_material.id)
            delivery_receipt = DeliveryReceipt.grab(self.delivery_receipt.id)
            location = Location.grab(self.location.id)
            client_for = Client.grab(delivery_receipt.client_for.id)
            client_from = Client.grab(delivery_receipt.client_from.id)

            no_pending = self.__class__.find({'_id': {'$ne': self._id}, 'delivery_receipt.$id': delivery_receipt.id, 'status': {'$lt': 2, '$gte': 0}}).count() == 0
            if no_pending:
                delivery_receipt.change_status(user_id, status=2)
            InventoryTransaction.create(
                created_by=user,
                type='delivery',
                raw_material=raw_material,
                price_per_unit=self['price_per_unit'],
                quality_control_no=self['quality_control_no'],
                location=location,
                expiry_date=self.get('expiry_date'),
                manufacturing_date=self.get('manufacturing_date'),
                cumulative_quantity=self['quantity'],
                delivery_receipt=delivery_receipt,
                client_for=client_for,
                client_from=client_from
            )
            kwargs['approved_by'] = user
        else:
            raise Exception('Validation - Invalid Target Status')
        kwargs[date_key] = date_now
        self.update(**kwargs)
        return date_key, date_now, user.human_readable()

    @classmethod
    def get_stock_on_hold(cls, raw_material, date_created=None):
        # NOTE: date_created for relative stock on hold
        if not isinstance(raw_material, RawMaterial):
            return -1
        quantity = 0
        filters = {'raw_material.$id': raw_material.id, 'status': {'$lt': 2, '$gte': 0}}
        if date_created:
            filters = {**filters, **{
                'date_created': {
                    '$lte': date_created
                }
            }}
        quantity += sum([rr.quantity for rr in cls.find(filters)])
        return quantity
