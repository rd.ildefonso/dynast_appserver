import arrow
from datetime import datetime
from common.mogo import Model, Field, ReferenceField
from models.user import User


class Client(Model):
    date_created = Field(datetime, required=True)
    created_by = ReferenceField(User, required=True)

    name = Field(str, required=True)
    address = Field(str, required=True)
    tin_number = Field(str)

    business_type = Field(str, required=True)
    primary_contact_number = Field(str)
    secondary_contact_number = Field(str)
    fax_number = Field(str)
    email_address = Field(str)

    contact_person = Field(str)

    @classmethod
    def create(cls, **kwargs):
        if not isinstance(kwargs.get('created_by'), User):
            try:
                kwargs['created_by'] = User.grab(kwargs.get('created_by'))
            except:
                raise Exception('Session has Expired')
        same_name = cls.find_one({'name': kwargs.get('name')})
        if same_name:
            name = kwargs.get('name')
            if name not in ['', None]:
                raise Exception(f'Validation - Record of Name "{name}" Already Exists')
        same_tin_number = cls.find_one({'tin_number': kwargs.get('tin_number')})
        if same_tin_number:
            tin_number = kwargs.get('tin_number')
            if tin_number not in ['', None]:
                raise Exception(f'Validation - Record with TIN "{tin_number}" Already Exists')
        same_primary_contact_number = cls.find_one({'primary_contact_number': kwargs.get('primary_contact_number')})
        if same_primary_contact_number:
            primary_contact_number = kwargs.get('primary_contact_number')
            if primary_contact_number not in ['', None]:
                raise Exception(f'Validation - Record with "{primary_contact_number}" Contact Already Exists')
        from models.data_setup import ClientBusinessType
        ds_business_type = ClientBusinessType.find_one({'value': kwargs.get('business_type')})
        if not isinstance(ds_business_type, ClientBusinessType):
            ClientBusinessType.create(value=kwargs.get('business_type'))
        result = super().create(**kwargs)
        r = cls.get_cache_connection()
        for key in r.scan_iter(f'api/v1/clients/all*'):
            r.delete(key)
        return result

    def update(self, **kwargs):
        if kwargs.get('name'):
            same_name = self.__class__.find_one({'name': kwargs.get('name'), '_id': {'$ne': self._id}})
            if same_name:
                name = kwargs.get('name')
                if name not in ['', None]:
                    raise Exception(f'Validation - Record of Name "{name}" Already Exists')
        if kwargs.get('tin_number'):
            same_tin_number = self.__class__.find_one({'tin_number': kwargs.get('tin_number'), '_id': {'$ne': self._id}})
            if same_tin_number:
                tin_number = kwargs.get('tin_number')
                if tin_number not in ['', None]:
                    raise Exception(f'Validation - Record with TIN "{tin_number}" Already Exists')
        if kwargs.get('primary_contact_number'):
            same_primary_contact_number = self.__class__.find_one({'primary_contact_number': kwargs.get('primary_contact_number'), '_id': {'$ne': self._id}})
            if same_primary_contact_number:
                primary_contact_number = kwargs.get('primary_contact_number')
                if primary_contact_number not in ['', None]:
                    raise Exception(f'Validation - Record with "{primary_contact_number}" Contact Already Exists')
        super().update(**kwargs)
        r = self.__class__.get_cache_connection()
        for key in r.scan_iter(f'api/v1/clients/all*'):
            r.delete(key)

    def delete(self):
        from models.inventory_transaction import InventoryTransaction
        last_transaction = InventoryTransaction.get_last_transaction(self)
        if last_transaction:
            raise Exception('Records Integrity - Transactions Exist')
        super().delete()
        r = self.__class__.get_cache_connection()
        for key in r.scan_iter(f'api/v1/clients/all*'):
            r.delete(key)

    @classmethod
    def datatable(cls, vars):
        sort, filter, all, filtered, paged = cls.process_datatable_params(vars)

        for index, item in enumerate(paged):
            paged[index] = dict(item)
            paged[index]['last_transaction'] = {'record': None, 'spiel': 'N/A'}
            from models.inventory_transaction import InventoryTransaction
            paged[index]['last_transaction']['record'] = InventoryTransaction.get_last_transaction(item)
            if paged[index]['last_transaction']['record']:
                last_transaction = paged[index]['last_transaction']['record']
                paged[index]['last_transaction']['spiel'] = last_transaction.human_readable(item)
        return {'data': paged, 'recordsTotal': all.count(), 'recordsFiltered': filtered.count()}
