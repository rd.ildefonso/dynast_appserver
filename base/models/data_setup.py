import arrow
from datetime import datetime
from common.mogo import PolyModel, Field, ReferenceField


"""
Contains soft linked records, meaning even without this collection, schema remains valid.
"""
class DataSetup(PolyModel):
    date_created = Field(datetime, required=True)

    type = Field(str, required=True)
    used_for = Field(str, required=True)

    @classmethod
    def create(cls, **kwargs):
        kwargs = dict({k: v.strip().lower() for k, v in kwargs.items()})
        data_setup = super().create(**kwargs)
        cls.cache_rebuild()
        return data_setup

    @classmethod
    def get_child_key(cls):
        return 'used_for'

    @classmethod
    def datatable(cls, vars):
        # START: Common
        columns = vars.get('columns', [])
        columns = {item.get('property'): True for item in columns if item.get('property', '') != ''}
        columns['_id'] = True
        required_filter = vars.pop('required_filter', {})
        limit = int(vars.get('length', 0))
        limit = 0 if limit < 0 else limit
        start = int(vars.get('start', 0))
        query_text = vars.get('search[value]', '')
        sort = [None for i in range(len(columns))]
        for k, v in {k: v for k, v in vars.items() if 'order[' in k and '][column]' in k}.items():
            sort_index = int(k.replace('order[', '').replace('][column]', ''))
            col_index = int(v)
            direction = 1 if vars.get(f'order[{sort_index}][dir]') == 'asc' else -1
            sort[sort_index] = (vars.get(f'columns[{col_index}][data]'), direction)
        sort = [s for s in sort if s != None]
        all = cls.find({}, columns)
        filter = {'$or': [{col.get('property'): {'$regex': query_text, '$options': '-i'}} for col in vars.get('columns', []) if col.get('regex') == True and query_text != '']}
        if filter['$or'] == []:
            filter.pop('$or')
        filter = {**required_filter, **filter}
        _data = cls.find(filter, columns)
        data = list(_data.sort(sort).limit(limit).skip(start))
        # END: Common
        return {'data': data, 'recordsTotal': all.count(), 'recordsFiltered': _data.count()}

    @classmethod
    def cache_rebuild(cls, clear=None):
        r = cls.get_cache_connection()
        r.flushall()
        cleared = []
        if clear:
            r.delete(clear)
            cleared.append(clear)
        for item in cls.find({'type': 'select'}).sort([('date_created', -1)]):
            item = dict(item)
            if item['used_for'] not in cleared:
                r.delete(item['used_for'])
                cleared.append(item['used_for'])
            r.lpush(item['used_for'], item['value'])


@DataSetup.register
class RawMaterialCategory(DataSetup):
    used_for = Field(str, default='raw_material_category')

    type = Field(str, default='select')
    value = Field(str, required=True)

    def update(self, **kwargs):
        kwargs = dict({k: v.strip().lower() for k, v in kwargs.items()})
        from models.raw_material import RawMaterial
        records_referencing_this = RawMaterial.find({'category': self.value})
        for record in records_referencing_this:
            record.update(category=kwargs['value'])
        super().update(**kwargs)
        self.__class__.cache_rebuild()

    def delete(self):
        from models.raw_material import RawMaterial
        record_referencing_this = RawMaterial.find_one({'category': self.value})
        if record_referencing_this:
            raise Exception('Records Integrity - Referenced by Existing Records')
        super().delete()
        if self.__class__.find().count() == 0:
            self.__class__.cache_rebuild(clear=self.used_for)
        self.__class__.cache_rebuild()


@DataSetup.register
class PackagingMaterialCategory(DataSetup):
    used_for = Field(str, default='packaging_material_category')

    type = Field(str, default='select')
    value = Field(str, required=True)

    def update(self, **kwargs):
        kwargs = dict({k: v.strip().lower() for k, v in kwargs.items()})
        from models.raw_material import RawMaterial
        records_referencing_this = RawMaterial.find({'category': self.value})
        for record in records_referencing_this:
            record.update(category=kwargs['value'])
        super().update(**kwargs)
        self.__class__.cache_rebuild()

    def delete(self):
        from models.raw_material import RawMaterial
        record_referencing_this = RawMaterial.find_one({'category': self.value})
        if record_referencing_this:
            raise Exception('Records Integrity - Referenced by Existing Records')
        super().delete()
        if self.__class__.find().count() == 0:
            self.__class__.cache_rebuild(clear=self.used_for)
        self.__class__.cache_rebuild()


@DataSetup.register
class RawMaterialUnit(DataSetup):
    used_for = Field(str, default='raw_material_unit')

    type = Field(str, default='select')
    value = Field(str, required=True)

    def update(self, **kwargs):
        kwargs = dict({k: v.strip().lower() for k, v in kwargs.items()})
        from models.raw_material import RawMaterial
        records_referencing_this = RawMaterial.find({'default_unit': self.value})
        for record in records_referencing_this:
            record.update(default_unit=kwargs['value'])
        super().update(**kwargs)
        self.__class__.cache_rebuild()

    def delete(self):
        from models.raw_material import RawMaterial
        record_referencing_this = RawMaterial.find_one({'default_unit': self.value})
        if record_referencing_this:
            raise Exception('Records Integrity - Referenced by Existing Records')
        super().delete()
        if self.__class__.find().count() == 0:
            self.__class__.cache_rebuild(clear=self.used_for)
        self.__class__.cache_rebuild()


@DataSetup.register
class ClientBusinessType(DataSetup):
    used_for = Field(str, default='client_business_type')

    type = Field(str, default='select')
    value = Field(str, required=True)

    def update(self, **kwargs):
        kwargs = dict({k: v.strip().lower() for k, v in kwargs.items()})
        from models.client import Client
        records_referencing_this = Client.find({'business_type': self.value})
        for record in records_referencing_this:
            record.update(business_type=kwargs['value'])
        super().update(**kwargs)
        self.__class__.cache_rebuild()

    def delete(self):
        from models.client import Client
        record_referencing_this = Client.find_one({'business_type': self.value})
        if record_referencing_this:
            raise Exception('Records Integrity - Referenced by Existing Records')
        super().delete()
        if self.__class__.find().count() == 0:
            self.__class__.cache_rebuild(clear=self.used_for)
        self.__class__.cache_rebuild()


@DataSetup.register
class FormulaUnit(DataSetup):
    used_for = Field(str, default='formula_unit')

    type = Field(str, default='select')
    value = Field(str, required=True)

    def update(self, **kwargs):
        kwargs = dict({k: v.strip().lower() for k, v in kwargs.items()})
        from models.formula import Formula
        records_referencing_this = Client.find({'default_unit': self.value})
        for record in records_referencing_this:
            record.update(default_unit=kwargs['value'])
        super().update(**kwargs)
        self.__class__.cache_rebuild()

    def delete(self):
        from models.formula import Formula
        record_referencing_this = Formula.find_one({'default_unit': self.value})
        if record_referencing_this:
            raise Exception('Records Integrity - Referenced by Existing Records')
        super().delete()
        if self.__class__.find().count() == 0:
            self.__class__.cache_rebuild(clear=self.used_for)
        self.__class__.cache_rebuild()
