import arrow
from datetime import datetime
from common.mogo import Model, Field, ReferenceField
from common.utils.generators import generate_alphanum


class FinishedGood(Model):
    date_created = Field(datetime, required=True)

    product_code = Field(str, required=True)
    description = Field(str, required=True)
    variant = Field(str)

    default_unit = Field(str, required=True)

    @classmethod
    def create(cls, **kwargs):
        kwargs['product_code'] = kwargs.get('product_code', generate_alphanum(6)).upper()
        same_product_code = cls.find_one({'product_code': kwargs.get('product_code')})
        if same_product_code:
            product_code = kwargs.get('product_code')
            if product_code not in ['', None]:
                raise Exception(f'Validation - Record of Name "{product_code}" Already Exists')
        return super().create(**kwargs)

    def update(self, **kwargs):
        if kwargs.get('product_code'):
            kwargs['product_code'] = kwargs['product_code'].upper()
            same_product_code = cls.find_one({'product_code': kwargs['product_code'], '_id': {'$ne': self._id}})
            if same_product_code:
                product_code = kwargs.get('product_code')
                if product_code not in ['', None]:
                    raise Exception(f'Validation - Record of Name "{product_code}" Already Exists')
        super().update(**kwargs)

    def delete(self):
        super().delete()

    @classmethod
    def datatable(cls, vars):
        # START: Common
        columns = vars.get('columns', [])
        columns = {item.get('property'): True for item in columns if item.get('property', '') != ''}
        columns['_id'] = True
        required_filter = vars.pop('required_filter', {})
        limit = int(vars.get('length', 0))
        limit = 0 if limit < 0 else limit
        start = int(vars.get('start', 0))
        query_text = vars.get('search[value]', '')
        sort = [None for i in range(len(columns))]
        for k, v in {k: v for k, v in vars.items() if 'order[' in k and '][column]' in k}.items():
            sort_index = int(k.replace('order[', '').replace('][column]', ''))
            col_index = int(v)
            direction = 1 if vars.get(f'order[{sort_index}][dir]') == 'asc' else -1
            sort[sort_index] = (vars.get(f'columns[{col_index}][data]'), direction)
        sort = [s for s in sort if s != None]
        all = cls.find({}, columns)
        filter = {'$or': [{col.get('property'): {'$regex': query_text, '$options': '-i'}} for col in vars.get('columns', []) if col.get('regex') == True and query_text != '']}
        if filter['$or'] == []:
            filter.pop('$or')
        filter = {**required_filter, **filter}
        _data = cls.find(filter, columns)
        data = list(_data.sort(sort).limit(limit).skip(start))
        # END: Common

        for index, item in enumerate(data):
            data[index] = dict(item)
            from models.finished_good_transaction import FinishedGoodTransaction
            data[index]['stock_on_hand'] = FinishedGoodTransaction.get_stock_on_hand(cls.grab(item['_id']))
            data[index]['last_transaction'] = FinishedGoodTransaction.get_last_transaction(cls.grab(item['_id']), humanize=True)
        return {'data': data, 'recordsTotal': all.count(), 'recordsFiltered': _data.count()}

    @classmethod
    def grab_by_product_code(cls, product_code):
        return cls.find_one({'product_code': str(product_code.upper())})
