import arrow
from settings import DATETIME_FORMAT, DEFAULT_TIMEZONE
from datetime import datetime
from common.mogo import Model, Field, ReferenceField
from models.client import Client
from models.user import User


class Formula(Model):
    date_created = Field(datetime, required=True)
    created_by = ReferenceField(User, required=True)
    date_original_issue = Field(datetime, required=True)
    last_modified_by = ReferenceField(User, required=True)

    formula_no = Field(str, required=True)
    client = ReferenceField(Client, required=True)
    product_code = Field(str, required=True)
    description = Field(str, required=True)
    variant = Field(str)

    revision_no = Field(int, required=True, default=1)
    revision_reason = Field(str)

    default_unit = Field(str, required=True)
    standard_packing_per_piece = Field(float, required=True)
    standard_specific_gravity = Field(float, required=True)
    valid = Field(bool, required=True)

    raw_materials = Field(list, default=[])
    packaging_materials = Field(list, default=[])

    encoded_to_finished_goods = Field(bool, default=False)

    @classmethod
    def create(cls, **kwargs):
        if not isinstance(kwargs.get('created_by'), User):
            try:
                kwargs['created_by'] = User.grab(kwargs.get('created_by'))
            except:
                raise Exception('Session has Expired')
        if not isinstance(kwargs['date_original_issue'], datetime):
            kwargs['date_original_issue'] = arrow.get(kwargs['date_original_issue'], 'MM/DD/YYYY').datetime
        if not isinstance(kwargs.get('last_modified_by'), User):
            try:
                kwargs['last_modified_by'] = User.grab(kwargs.get('last_modified_by'))
            except:
                raise Exception('Session has Expired')
        if not isinstance(kwargs.get('client'), Client):
            try:
                kwargs['client'] = Client.grab(kwargs.get('client'))
            except:
                raise Exception('Validation - Invalid Client')
        if not isinstance(kwargs.get('valid'), bool):
            kwargs['valid'] = True if kwargs.get('valid').lower() == 'true' else False
        if kwargs['valid']:
            from models.finished_good import FinishedGood
            FinishedGood.create(
                product_code=kwargs['product_code'],
                description=kwargs['description'],
                variant=kwargs.get('variant', ''),
                default_unit=kwargs['default_unit']
            )
            kwargs['encoded_to_finished_goods'] = True
        if not isinstance(kwargs.get('revision_no'), int):
            kwargs['revision_no'] = int(kwargs['revision_no'])
        kwargs['standard_packing_per_piece'] = float(kwargs['standard_packing_per_piece'])
        kwargs['standard_specific_gravity'] = float(kwargs['standard_specific_gravity'])
        from models.data_setup import FormulaUnit
        ds_unit = FormulaUnit.find_one({'value': kwargs.get('default_unit')})
        if not isinstance(ds_unit, FormulaUnit):
            FormulaUnit.create(value=kwargs.get('default_unit'))
        result = super().create(**kwargs)
        r = cls.get_cache_connection()
        for key in r.scan_iter(f'api/v1/formulas/all*'):
            r.delete(key)
        return result

    def update(self, **kwargs):
        from models.finished_good import FinishedGood
        kwargs['revision_no'] = self.revision_no + 1
        if not isinstance(kwargs['last_modified_by'], User):
            kwargs['last_modified_by'] = User.grab(kwargs['last_modified_by'])
        if kwargs.get('date_original_issue'):
            if not isinstance(kwargs['date_original_issue'], datetime):
                kwargs['date_original_issue'] = arrow.get(kwargs['date_original_issue'], 'MM/DD/YYYY').datetime
        if not isinstance(kwargs['client'], Client):
            kwargs['client'] = Client.grab(kwargs['client'])
        if not isinstance(kwargs['valid'], bool):
            kwargs['valid'] = True if kwargs['valid'].lower() == 'true' else False
        if kwargs['valid'] and not self.encoded_to_finished_goods:
            kwargs['encoded_to_finished_goods'] = True
            FinishedGood.create(
                product_code=kwargs['product_code'],
                description=kwargs['description'],
                variant=kwargs.get('variant', ''),
                default_unit=kwargs['default_unit']
            )
        if self.encoded_to_finished_goods:
            finished_good = FinishedGood.grab_by_product_code(kwargs['product_code'])
            finished_good.update(
                description=kwargs['description'],
                variant=kwargs.get('variant', ''),
                default_unit=kwargs['default_unit']
            )
        kwargs['standard_packing_per_piece'] = float(kwargs['standard_packing_per_piece'])
        kwargs['standard_specific_gravity'] = float(kwargs['standard_specific_gravity'])
        if kwargs.get('date_created'):
            if not isinstance(kwargs['date_created'], datetime):
                kwargs['date_created'] = arrow.get(kwargs['date_created'], 'MM/DD/YYYY').datetime
        if kwargs.get('revision_no'):
            if not isinstance(kwargs.get('revision_no'), int):
                kwargs['revision_no'] = int(kwargs['revision_no'])
        super().update(**kwargs)
        r = self.__class__.get_cache_connection()
        for key in r.scan_iter(f'api/v1/formulas/all*'):
            r.delete(key)

    def delete(self):
        from models.finished_good import FinishedGood
        finished_good = FinishedGood.grab_by_product_code(self.product_code)
        if not isinstance(finished_good, FinishedGood):
            raise Exception('Malformed Records - FG Does not Exist')
        from models.finished_good_transaction import FinishedGoodTransaction
        stock_on_hand = FinishedGoodTransaction.get_stock_on_hand(finished_good)
        if stock_on_hand > 0:
            raise Exception('Records Integrity - Stock Exists')
        finished_good.delete()
        super().delete()
        r = self.__class__.get_cache_connection()
        for key in r.scan_iter(f'api/v1/formulas/all*'):
            r.delete(key)

    @classmethod
    def datatable(cls, vars):
        sort, filter, all, filtered, paged = cls.process_datatable_params(vars)

        for index, item in enumerate(paged):
            paged[index] = dict(item)
            paged[index]['client'] = Client.grab(item['client'].id)['name']
            if 'raw_materials' in item:
                paged[index]['raw_materials'] = len(item.pop('raw_materials'))
            if 'packaging_materials' in item:
                paged[index]['packaging_materials'] = len(item.pop('packaging_materials'))
            paged[index]['date_last_modified'] = arrow.get(item['date_last_modified']).to(DEFAULT_TIMEZONE).datetime.strftime(DATETIME_FORMAT)
        return {'data': paged, 'recordsTotal': all.count(), 'recordsFiltered': filtered.count()}
