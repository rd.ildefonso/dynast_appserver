import arrow
from datetime import datetime
from common.mogo import Model, Field, ReferenceField
from models.raw_material import RawMaterial
from models.user import User


class IssueTag(Model):
    date_created = Field(datetime, required=True)
    created_by = ReferenceField(User, required=True)

    raw_material = ReferenceField(RawMaterial, required=True)
    target_stock_level = Field(float, required=True)
    remarks = Field(str, required=True)

    @classmethod
    def create(cls, **kwargs):
        if not isinstance(kwargs.get('created_by'), User):
            try:
                kwargs['created_by'] = User.grab(kwargs.get('created_by'))
            except:
                raise Exception('Session has Expired')
        if not isinstance(kwargs['raw_material'], RawMaterial):
            kwargs['raw_material'] = RawMaterial.grab(kwargs['raw_material'])
        kwargs['target_stock_level'] = float(kwargs['target_stock_level'])
        if kwargs['target_stock_level'] < 0:
            raise Exception('Validation - Invalid Target Stock Level')
        kwargs['target_stock_level'] = round(kwargs['target_stock_level'], 4)
        issue_tag = super().create(**kwargs)
        from models.inventory_transaction import InventoryTransaction
        InventoryTransaction.create(
            created_by=kwargs['created_by'],
            type='issue_tag',
            raw_material=kwargs['raw_material'],
            aggregate_quantity=kwargs['target_stock_level'],
            issue_tag=issue_tag
        )
        return issue_tag
