import arrow
from settings import DATETIME_FORMAT, DEFAULT_TIMEZONE
from datetime import datetime
from common.mogo import Model, Field, ReferenceField
from models.client import Client
from models.user import User


class OutgoingDelivery(Model):
    date_created = Field(datetime, required=True)
    created_by = ReferenceField(User, required=True)

    outgoing_delivery_no = Field(str, required=True)

    products = Field(list, default=[])
    client = ReferenceField(Client, required=True)

    # NOTE: 0 - Draft / 1 - For Payment (Reserved) / 2 - For Delivery (Paid) / 3 - In Transit / 4 - Completed
    status = Field(int, required=True, default=0)
    reserved_by = ReferenceField(User)
    date_reserved = Field(datetime)
    paid_by = ReferenceField(User)
    date_paid = Field(datetime)
    transit_by = ReferenceField(User)
    date_transit = Field(datetime)
    completed_by = ReferenceField(User)
    date_completed = Field(datetime)

    @classmethod
    def create(cls, **kwargs):
        if not isinstance(kwargs.get('created_by'), User):
            try:
                kwargs['created_by'] = User.grab(kwargs.get('created_by'))
            except:
                raise Exception('Session has Expired')
        if not isinstance(kwargs['client'], Client):
            kwargs['client'] = Client.grab(kwargs['client'])
        if kwargs.get('products'):
            from models.finished_good import FinishedGood
            for index, item in enumerate(kwargs['products']):
                finished_good = FinishedGood.grab(kwargs['products'][index]['product_code'])
                if not isinstance(finished_good, FinishedGood):
                    log.error(f'Skipped Invalid Finished Good Product Code - "{product_code}""')
                    continue
                kwargs['products'][index]['quantity'] = round(float(kwargs['products'][index]['quantity']), 4)
                try:
                    kwargs['products'][index]['price_per_unit'] = round(float(kwargs['products'][index]['price_per_unit']), 2)
                except:
                    kwargs['products'][index]['price_per_unit'] = 0
        return super().create(**kwargs)

    def update(self, **kwargs):
        if not isinstance(kwargs['client'], Client):
            kwargs['client'] = Client.grab(kwargs['client'])
        if kwargs.get('products'):
            from models.finished_good import FinishedGood
            for index, item in enumerate(kwargs['products']):
                finished_good = FinishedGood.grab(kwargs['products'][index]['product_code'])
                if not isinstance(finished_good, FinishedGood):
                    log.error(f'Skipped Invalid Finished Good Product Code - "{product_code}""')
                    continue
                kwargs['products'][index]['quantity'] = round(float(kwargs['products'][index]['quantity']), 4)
                try:
                    kwargs['products'][index]['price_per_unit'] = round(float(kwargs['products'][index]['price_per_unit']), 2)
                except:
                    kwargs['products'][index]['price_per_unit'] = 0
        super().update(**kwargs)

    @classmethod
    def datatable(cls, vars):
        sort, filter, all, filtered, paged = cls.process_datatable_params(vars)

        for index, item in enumerate(paged):
            paged[index] = dict(item)
            if 'client' in item:
                paged[index]['client'] = dict(Client.grab(item['client'].id))
            paged[index]['date_created'] = arrow.get(item['date_created']).to(DEFAULT_TIMEZONE).datetime.strftime(DATETIME_FORMAT)
        return {'data': paged, 'recordsTotal': all.count(), 'recordsFiltered': filtered.count()}

    def change_status(self, user_id, **kwargs):
        # try:
        #     kwargs['status'] = int(kwargs['status'])
        # except:
        #     raise Exception('Validation - Invalid Target Status')
        #
        # user = User.grab(user_id)
        # date_now = arrow.utcnow().datetime
        # date_key = ''
        # if kwargs['status'] == 1:
        #     date_key = 'date_submitted'
        #     items = self['items']
        #     if len(items) == 0:
        #         raise Exception('Validation - No Items Defined')
        #     for item in items:
        #         from models.raw_material import RawMaterial
        #         item_code = item['item_code']
        #         raw_material = RawMaterial.grab(item_code)
        #         if not isinstance(raw_material, RawMaterial):
        #             log.error(f'Skipped Invalid Raw Material Code - "{item_code}""')
        #             continue
        #         from models.receiving_report import ReceivingReport
        #         data = {
        #             'raw_material': raw_material,
        #             'delivery_receipt': self,
        #             'quantity': item['quantity'],
        #             'price_per_unit': item['price_per_unit'],
        #             'status': 0
        #         }
        #         if item.get('expiry_date') not in ['', None]:
        #             data['expiry_date'] = item['expiry_date']
        #         if item.get('manufacturing_date') not in ['', None]:
        #             data['manufacturing_date'] = item['manufacturing_date']
        #         ReceivingReport.create(**data)
        #     kwargs['submitted_by'] = user
        # elif kwargs['status'] == 2:
        #     date_key = 'date_completed'
        # else:
        #     raise Exception('Validation - Invalid Target Status')
        # kwargs[date_key] = date_now
        self.update(**kwargs)
