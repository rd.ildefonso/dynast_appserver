import arrow
from settings import DATETIME_FORMAT, DEFAULT_TIMEZONE
from datetime import datetime
from common.mogo import Model, Field, ReferenceField
from models.client import Client
from models.user import User


class DeliveryReceipt(Model):
    date_created = Field(datetime, required=True)
    created_by = ReferenceField(User, required=True)

    date_delivered = Field(datetime, required=True)
    receipt_no = Field(str, required=True)
    purchase_order_no = Field(str, required=True)

    items = Field(list, default=[])
    client_for = ReferenceField(Client, required=True)
    client_from = ReferenceField(Client, required=True)

    # NOTE: 0 - Draft / 1 - Review Pending/Ongoing / 2 - Completed / -1 - Void
    status = Field(int, required=True, default=0)
    delivered_by = ReferenceField(User)
    submitted_by = ReferenceField(User)
    date_submitted = Field(datetime)
    date_completed = Field(datetime)

    @classmethod
    def create(cls, **kwargs):
        if not isinstance(kwargs.get('created_by'), User):
            try:
                kwargs['created_by'] = User.grab(kwargs.get('created_by'))
            except:
                raise Exception('Session has Expired')
        if not isinstance(kwargs['date_delivered'], datetime):
            kwargs['date_delivered'] = arrow.get(kwargs['date_delivered'], 'MM/DD/YYYY').datetime
        if not isinstance(kwargs['client_from'], Client):
            kwargs['client_from'] = Client.grab(kwargs['client_from'])
        if not isinstance(kwargs['client_for'], Client):
            kwargs['client_for'] = Client.grab(kwargs['client_for'])
        if kwargs.get('items'):
            from models.raw_material import RawMaterial
            for index, item in enumerate(kwargs['items']):
                raw_material = RawMaterial.grab(kwargs['items'][index]['item_code'])
                if not isinstance(raw_material, RawMaterial):
                    log.error(f'Skipped Invalid Raw Material Code - "{item_code}""')
                    continue
                kwargs['items'][index]['quantity'] = round(float(kwargs['items'][index]['quantity']), 4)
                try:
                    kwargs['items'][index]['price_per_unit'] = round(float(kwargs['items'][index]['price_per_unit']), 2)
                except:
                    kwargs['items'][index]['price_per_unit'] = 0
        return super().create(**kwargs)

    def update(self, **kwargs):
        if kwargs.get('date_delivered'):
            if not isinstance(kwargs['date_delivered'], datetime):
                kwargs['date_delivered'] = arrow.get(kwargs['date_delivered'], 'MM/DD/YYYY').datetime
        if kwargs.get('client_from'):
            if not isinstance(kwargs['client_from'], Client):
                kwargs['client_from'] = Client.grab(kwargs['client_from'])
        if kwargs.get('client_for'):
            if not isinstance(kwargs['client_for'], Client):
                kwargs['client_for'] = Client.grab(kwargs['client_for'])
        if kwargs.get('items'):
            from models.raw_material import RawMaterial
            for index, item in enumerate(kwargs['items']):
                raw_material = RawMaterial.grab(kwargs['items'][index]['item_code'])
                if not isinstance(raw_material, RawMaterial):
                    # log.error(f'Skipped Invalid Raw Material Code - "{item_code}""')
                    continue
                kwargs['items'][index]['quantity'] = round(float(kwargs['items'][index]['quantity']), 4)
                try:
                    kwargs['items'][index]['price_per_unit'] = round(float(kwargs['items'][index]['price_per_unit']), 2)
                except:
                    kwargs['items'][index]['price_per_unit'] = 0
        super().update(**kwargs)

    @classmethod
    def datatable(cls, vars):
        sort, filter, all, filtered, paged = cls.process_datatable_params(vars)

        for index, item in enumerate(paged):
            paged[index] = dict(item)
            if 'client_from' in item:
                paged[index]['client_from'] = dict(Client.grab(item['client_from'].id))
            if 'client_for' in item:
                paged[index]['client_for'] = dict(Client.grab(item['client_for'].id))
            paged[index]['date_created'] = arrow.get(item['date_created']).to(DEFAULT_TIMEZONE).datetime.strftime(DATETIME_FORMAT)
            paged[index]['date_delivered'] = arrow.get(item['date_delivered']).to(DEFAULT_TIMEZONE).datetime.strftime('%Y-%m-%d')
        return {'data': paged, 'recordsTotal': all.count(), 'recordsFiltered': filtered.count()}

    def change_status(self, user_id, **kwargs):
        try:
            kwargs['status'] = int(kwargs['status'])
        except:
            raise Exception('Validation - Invalid Target Status')

        user = User.grab(user_id)
        date_now = arrow.utcnow().datetime
        date_key = ''
        if kwargs['status'] == 1:
            date_key = 'date_submitted'
            items = self['items']
            if len(items) == 0:
                raise Exception('Validation - No Items Defined')
            for item in items:
                from models.raw_material import RawMaterial
                item_code = item['item_code']
                raw_material = RawMaterial.grab(item_code)
                if not isinstance(raw_material, RawMaterial):
                    log.error(f'Skipped Invalid Raw Material Code - "{item_code}""')
                    continue
                from models.receiving_report import ReceivingReport
                data = {
                    'raw_material': raw_material,
                    'delivery_receipt': self,
                    'quantity': item['quantity'],
                    'price_per_unit': item['price_per_unit'],
                    'status': 0
                }
                if item.get('expiry_date') not in ['', None]:
                    data['expiry_date'] = item['expiry_date']
                if item.get('manufacturing_date') not in ['', None]:
                    data['manufacturing_date'] = item['manufacturing_date']
                ReceivingReport.create(**data)
            kwargs['submitted_by'] = user
        elif kwargs['status'] == 2:
            date_key = 'date_completed'
        else:
            raise Exception('Validation - Invalid Target Status')
        kwargs[date_key] = date_now
        self.update(**kwargs)
