import arrow
from datetime import datetime
from common.mogo import Model, Field, ReferenceField
from common.utils.generators import generate_alphanum
from models.user import User


class RawMaterial(Model):
    date_created = Field(datetime, required=True)
    created_by = ReferenceField(User, required=True)

    item_code = Field(str, required=True)
    description = Field(str, required=True)

    category = Field(str, required=True)
    default_unit = Field(str, required=True)

    @classmethod
    def create(cls, **kwargs):
        if not isinstance(kwargs.get('created_by'), User):
            try:
                kwargs['created_by'] = User.grab(kwargs.get('created_by'))
            except:
                raise Exception('Session has Expired')
        kwargs['item_code'] = kwargs.get('item_code', generate_alphanum(6)).upper()
        same_item_code = cls.find_one({'item_code': kwargs.get('item_code')})
        if same_item_code:
            item_code = kwargs.get('item_code')
            if item_code != '':
                raise Exception(f'Validation - Record of Name "{item_code}" Already Exists')
        from models.data_setup import RawMaterialUnit
        # ds_category = RawMaterialCategory.find_one({'value': kwargs.get('category')})
        # if not isinstance(ds_category, RawMaterialCategory):
        #     RawMaterialCategory.create(value=kwargs.get('category'))
        ds_unit = RawMaterialUnit.find_one({'value': kwargs.get('default_unit')})
        if not isinstance(ds_unit, RawMaterialUnit):
            RawMaterialUnit.create(value=kwargs.get('default_unit'))
        result = super().create(**kwargs)
        r = cls.get_cache_connection()
        for key in r.scan_iter(f'api/v1/raw_materials/all*'):
            r.delete(key)
        return result

    def update(self, **kwargs):
        if kwargs.get('item_code'):
            kwargs['item_code'] = kwargs['item_code'].upper()
            same_item_code = self.__class__.find_one({'item_code': kwargs['item_code'], '_id': {'$ne': self._id}})
            if same_item_code:
                item_code = kwargs['item_code']
                raise Exception(f'Validation - Record of Name "{item_code}" Already Exists')
        from models.data_setup import RawMaterialCategory, RawMaterialUnit
        # if kwargs.get('category'):
        #     ds_category = RawMaterialCategory.find_one({'value': kwargs.get('category')})
        #     if not isinstance(ds_category, RawMaterialCategory):
        #         RawMaterialCategory.create(value=kwargs.get('category'))
        if kwargs.get('default_unit'):
            ds_unit = RawMaterialUnit.find_one({'value': kwargs.get('default_unit')})
            if not isinstance(ds_unit, RawMaterialUnit):
                RawMaterialUnit.create(value=kwargs.get('default_unit'))
        super().update(**kwargs)
        r = self.__class__.get_cache_connection()
        for key in r.scan_iter(f'api/v1/raw_materials/all*'):
            r.delete(key)

    def delete(self):
        from models.inventory_transaction import InventoryTransaction
        from models.receiving_report import ReceivingReport
        stock_on_hand = InventoryTransaction.get_stock_on_hand(self)
        stock_on_hold = ReceivingReport.get_stock_on_hold(self)
        if any([stock_on_hand > 0, stock_on_hold > 0]):
            raise Exception('Records Integrity - Stock Exists')
        super().delete()
        r = self.__class__.get_cache_connection()
        for key in r.scan_iter(f'api/v1/raw_materials/all*'):
            r.delete(key)

    @classmethod
    def datatable(cls, vars):
        sort, filter, all, filtered, paged = cls.process_datatable_params(vars)

        for index, item in enumerate(paged):
            paged[index] = dict(item)
            paged[index]['last_transaction'] = {'record': None, 'spiel': 'N/A'}
            from models.inventory_transaction import InventoryTransaction
            paged[index]['stock_on_hand'] = InventoryTransaction.get_stock_on_hand(item)
            paged[index]['last_transaction']['record'] = InventoryTransaction.get_last_transaction(item)
            # TODO: Sort Post
            if paged[index]['last_transaction']['record']:
                # print(paged[index]['last_transaction']['record']['date_created'])
                # print(type(paged[index]['last_transaction']['record']['date_created']))
                # print(paged[index]['last_transaction']['record']['date_created'].total_seconds())
                # epoch = arrow.get('1970-01-01 00:00:00 UTC').datetime
                # print((epoch - paged[index]['last_transaction']['record']['date_created']).total_seconds())
                user = paged[index]['last_transaction']['record']
                paged[index]['last_transaction']['spiel'] = user.human_readable(item) if user else ''
        for s in sort:
            reverse = s[1] == -1
            if s[0] == 'last_transaction':
                epoch = arrow.get('1970-01-01 00:00:00 UTC').datetime
                # paged.sort(key=lambda d: (epoch - d.get('record', {}).get('date_created', epoch)).total_seconds(), reverse=reverse)
                continue
            paged.sort(key=lambda d: d[s[0]], reverse=reverse)
        return {'data': paged, 'recordsTotal': all.count(), 'recordsFiltered': filtered.count()}

    @classmethod
    def grab_by_item_code(cls, item_code):
        return cls.find_one({'item_code': str(item_code.upper())})
