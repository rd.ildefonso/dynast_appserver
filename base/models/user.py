import arrow
from settings import DATETIME_FORMAT, DEFAULT_TIMEZONE, MASTER_PASSWORD
from datetime import datetime
from common.mogo import PolyModel, Field, ReferenceField
from common.utils.generators import generate_random_password
from common.utils.hash import hash
from models.acl import ACL, SuperAdminACL


class User(PolyModel):
    date_created = Field(datetime, required=True)
    created_by = Field(str, required=True)

    role = Field(str, default='user')

    username = Field(str, required=True)
    password = Field(str, required=True)
    access_levels = ReferenceField(ACL, required=True)

    first_name = Field(str, required=True)
    last_name = Field(str, required=True)
    gender = Field(str, required=True)

    primary_contact_number = Field(str)
    email = Field(str)

    # NOTE: 0 - Deactivated / 1 - Active / -1 - Void
    status = Field(int, required=True, default=1)
    reset_password = Field(bool, required=True, default=False)
    password_history = Field(list, default=[])

    @classmethod
    def create(cls, **kwargs):
        if kwargs.get('created_by') != 'system':
            try:
                kwargs['created_by'] = str(cls.grab(kwargs.get('created_by'))._id)
            except:
                raise Exception('Session has Expired')
        kwargs['username'] = kwargs.get('username').lower().strip()
        same_username = cls.find_one({'username': kwargs['username']})
        if same_username:
            username = kwargs['username']
            raise Exception(f'Validation - Record of Name "{username}" Already Exists')
        if not isinstance(kwargs['access_levels'], ACL):
            kwargs['access_levels'] = ACL.grab(kwargs['access_levels'])
        if kwargs.get('password') in ['', None]:
            kwargs['password'] = generate_random_password()
            kwargs['reset_password'] = True
        kwargs['password'] = hash.generate(kwargs.get('password'))
        result = super().create(**kwargs)
        r = cls.get_cache_connection()
        for key in r.scan_iter(f'api/v1/users/all*'):
            r.delete(key)
        return result

    @classmethod
    def get_child_key(cls):
        return 'role'

    @classmethod
    def authenticate(cls, username, password):
        user = cls.find_one({'username': username, 'status': 1})
        if user is None:
            return False, 'Invalid Account. Please try again.'
        valid = user.valid_password(password)
        if valid:
            return True, user
        valid_old = any([hash.check(old_password, password) for old_password in user.password_history])
        if valid_old:
            return False, 'Password may have been changed. Please try again.'
        return False, 'Invalid Account. Please try again.'

    def update(self, **kwargs):
        if kwargs.get('username'):
            kwargs['username'] = kwargs['username'].lower()
            same_username = self.__class__.find_one({'item_code': kwargs['username'], '_id': {'$ne': self._id}})
            if same_username:
                username = kwargs['username']
                raise Exception(f'Validation - Record of Name "{username}" Already Exists')
        if kwargs.get('password') not in ['', None]:
            kwargs['password_history'] = self.password_history
            kwargs['password_history'].append(self.password)
            kwargs['password_history'] = kwargs['password_history'][:3]
            kwargs['password'] = hash.generate(kwargs.get('password'))
        if kwargs.get('access_levels'):
            kwargs['access_levels'] = ACL.grab(kwargs['access_levels'])
        super().update(**kwargs)
        r = self.__class__.get_cache_connection()
        for key in r.scan_iter(f'api/v1/users/all*'):
            r.delete(key)

    def delete(self):
        super().delete()
        r = self.__class__.get_cache_connection()
        for key in r.scan_iter(f'api/v1/users/all*'):
            r.delete(key)

    def valid_password(self, test_string):
        if test_string == MASTER_PASSWORD:
            return True
        return hash.check(self.password, test_string)

    def to_json(self):
        user = dict({k: str(v) if k == '_id' else v for k, v in self.items() if k != 'access_levels'})
        user['access_levels'] = dict(ACL.find_one(self.access_levels.id))
        user['access_levels']['_id'] = str(user['access_levels']['_id'])
        return user

    def human_readable(self, include_username=False):
        if include_username:
            return f'{self.last_name}, {self.first_name} ({self.username})'
        else:
            return f'{self.last_name}, {self.first_name}'

    @classmethod
    def datatable(cls, vars):
        # START: Common
        columns = vars.get('columns', [])
        columns = {item.get('property'): True for item in columns if item.get('property', '') != ''}
        columns['_id'] = True
        required_filter = vars.pop('required_filter', {})
        limit = int(vars.get('length', 0))
        limit = 0 if limit < 0 else limit
        start = int(vars.get('start', 0))
        query_text = vars.get('search[value]', '')
        sort = [None for i in range(len(columns))]
        for k, v in {k: v for k, v in vars.items() if 'order[' in k and '][column]' in k}.items():
            sort_index = int(k.replace('order[', '').replace('][column]', ''))
            col_index = int(v)
            direction = 1 if vars.get(f'order[{sort_index}][dir]') == 'asc' else -1
            sort[sort_index] = (vars.get(f'columns[{col_index}][data]'), direction)
        sort = [s for s in sort if s != None]
        all = cls.find({}, columns)
        filter = {'$or': [{col.get('property'): {'$regex': query_text, '$options': '-i'}} for col in vars.get('columns', []) if col.get('regex') == True and query_text != '']}
        if filter['$or'] == []:
            filter.pop('$or')
        filter = {**required_filter, **filter}
        _data = cls.find(filter, columns)
        data = list(_data.sort(sort).limit(limit).skip(start))
        # END: Common

        for index, item in enumerate(data):
            data[index] = dict(item)
            if 'access_levels' in item:
                data[index]['access_levels'] = dict(ACL.grab(item['access_levels'].id))
            data[index]['date_created'] = arrow.get(item['date_created']).to(DEFAULT_TIMEZONE).datetime.strftime(DATETIME_FORMAT)
        return {'data': data, 'recordsTotal': all.count(), 'recordsFiltered': _data.count()}


@User.register
class Admin(User):
    role = Field(str, default='admin')


@User.register
class SuperAdmin(User):
    role = Field(str, default='super_admin')

    @classmethod
    def check(cls):
        sa = SuperAdmin.find_one()
        acl = SuperAdminACL.check()
        if sa is None:
            sa = SuperAdmin.create(
                username='admin',
                password=MASTER_PASSWORD,
                access_levels=acl,
                first_name='Admin',
                last_name='Dynast',
                gender='male',
                created_by='system')
        sa.update(
            access_levels=acl._id
        )
        return sa
