import arrow
from common.mogo import connect
from settings import APPSERVER_NAME, MONGODB

connect(APPSERVER_NAME, uri=f'mongodb://{MONGODB["host"]}:{MONGODB["port"]}')


def initialize():
    from .user import SuperAdmin
    from .data_setup import DataSetup
    SuperAdmin.check()
    DataSetup.cache_rebuild()
