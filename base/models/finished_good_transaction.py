import arrow
from settings import DATETIME_FORMAT, DEFAULT_TIMEZONE
from datetime import datetime
from common.mogo import Model, Field, ReferenceField
from models.finished_good import FinishedGood
from models.user import User
from models.location import Location


class FinishedGoodTransaction(Model):
    date_created = Field(datetime, required=True)
    created_by = ReferenceField(User, required=True)

    # NOTE: stock_transfer (STS) / purchase (PO)
    type = Field(str, required=True)

    finished_good = ReferenceField(FinishedGood, required=True)
    quantity = Field(float, required=True)

    location = ReferenceField(Location)
    lot_number = Field(str)
    expiry_date = Field(datetime)
    manufacturing_date = Field(datetime)

    purchase_date = Field(datetime)
    price_per_unit = Field(float)

    @classmethod
    def create(cls, **kwargs):
        if not isinstance(kwargs['created_by'], User):
            kwargs['created_by'] = User.grab(kwargs['created_by'])
        if not isinstance(kwargs['finished_good'], FinishedGood):
            kwargs['finished_good'] = FinishedGood.grab(kwargs['finished_good'])
        kwargs['quantity'] = float(kwargs['quantity'])
        if kwargs.get('location'):
            if not isinstance(kwargs['location'], Location):
                kwargs['location'] = Location.grab(kwargs['location'])
        if kwargs.get('expiry_date', '') != '':
            if not isinstance(kwargs['expiry_date'], datetime):
                try:
                    kwargs['expiry_date'] = arrow.get(kwargs['expiry_date']).datetime
                except:
                    kwargs.pop('expiry_date')
        else:
            kwargs.pop('expiry_date', None)
        if kwargs.get('manufacturing_date', '') != '':
            if not isinstance(kwargs['manufacturing_date'], datetime):
                try:
                    kwargs['manufacturing_date'] = arrow.get(kwargs['manufacturing_date']).datetime
                except:
                    kwargs.pop('manufacturing_date')
        else:
            kwargs.pop('manufacturing_date', None)
        return super().create(**kwargs)

    @classmethod
    def get_stock_on_hand(cls, finished_good, date_created=None):
        # NOTE: date_created for relative stock on hand
        if not isinstance(finished_good, FinishedGood):
            return -1
        quantity = 0
        filters = {'finished_good.$id': finished_good.id}
        if date_created:
            filters = {**filters, **{
                'date_created': {
                    '$lte': date_created
                }
            }}
        quantity += sum([fgt.quantity if fgt.type == 'stock_transfer' else (0-(2*fgt.quantity)) for fgt in cls.find(filters)])
        return quantity

    def get_relative_stock_on_hand(self):
        return self.__class__.get_stock_on_hand(FinishedGood.grab(self.finished_good.id), date_created=self.date_created)

    @classmethod
    def get_last_transaction(cls, reference, humanize=False):
        last_transaction = None
        if isinstance(reference, FinishedGood):
            _last_transaction = list(cls.find({'finished_good.$id': reference.id}).sort([('date_created', -1)]).limit(1))
            if _last_transaction:
                last_transaction = _last_transaction[0]
                if humanize:
                    if last_transaction.type == 'stock_transfer':
                        from models.location import Location
                        location = Location.grab(last_transaction.location._id)
                        return f'{last_transaction.quantity} stocks transfered to {location.name}'
                    elif last_transaction.type == 'purchase':
                        return f'TODO'
                    else:
                        return 'Cannot be determined'
            else:
                if humanize:
                    return 'N/A'
            return last_transaction

    @classmethod
    def datatable(cls, vars):
        # START: Common
        columns = vars.get('columns', [])
        columns = {item.get('property'): True for item in columns if item.get('property', '') != ''}
        columns['_id'] = True
        required_filter = vars.pop('required_filter', {})
        limit = int(vars.get('length', 0))
        limit = 0 if limit < 0 else limit
        start = int(vars.get('start', 0))
        query_text = vars.get('search[value]', '')
        sort = [None for i in range(len(columns))]
        for k, v in {k: v for k, v in vars.items() if 'order[' in k and '][column]' in k}.items():
            sort_index = int(k.replace('order[', '').replace('][column]', ''))
            col_index = int(v)
            direction = 1 if vars.get(f'order[{sort_index}][dir]') == 'asc' else -1
            sort[sort_index] = (vars.get(f'columns[{col_index}][data]'), direction)
        sort = [s for s in sort if s != None]
        all = cls.find({}, columns)
        filter = {'$or': [{col.get('property'): {'$regex': query_text, '$options': '-i'}} for col in vars.get('columns', []) if col.get('regex') == True and query_text != '']}
        if filter['$or'] == []:
            filter.pop('$or')
        filter = {**required_filter, **filter}
        _data = cls.find(filter, columns)
        data = list(_data.sort(sort).limit(limit).skip(start))
        # END: Common

        for index, item in enumerate(data):
            data[index] = dict(item)
            if item['type'] == 'stock_transfer':
                if 'location' in item:
                    data[index]['location'] = Location.grab(item.location.id)['name']
            elif item['type'] == 'purchase':
                pass
            data[index]['date_created'] = arrow.get(item['date_created']).to(DEFAULT_TIMEZONE).datetime.strftime(DATETIME_FORMAT)
            created_by = User.grab(item.created_by.id)
            data[index]['created_by'] = created_by.human_readable() if created_by else ''
        return {'data': data, 'recordsTotal': all.count(), 'recordsFiltered': _data.count()}
