import arrow
from datetime import datetime
from common.mogo import Model, Field, ReferenceField
from common.utils.generators import generate_alphanum
from models.user import User


class Location(Model):
    date_created = Field(datetime, required=True)
    created_by = ReferenceField(User, required=True)

    name = Field(str, required=True)
    address = Field(str, required=True)

    @classmethod
    def create(cls, **kwargs):
        if not isinstance(kwargs.get('created_by'), User):
            try:
                kwargs['created_by'] = User.grab(kwargs.get('created_by'))
            except:
                raise Exception('Session has Expired')
        same_name = cls.find_one({'name': kwargs['name']})
        if same_name:
            name = kwargs['name']
            raise Exception(f'Validation - Record of Name "{name}" Already Exists')
        result = super().create(**kwargs)
        r = cls.get_cache_connection()
        for key in r.scan_iter(f'api/v1/locations/all*'):
            r.delete(key)
        return result

    def update(self, **kwargs):
        if kwargs.get('name'):
            same_name = self.__class__.find_one({'name': kwargs.get('name'), '_id': {'$ne': self._id}})
            if same_name:
                name = kwargs.get('name')
                raise Exception(f'Validation - Record of Name "{name}" Already Exists')
        super().update(**kwargs)
        r = self.__class__.get_cache_connection()
        for key in r.scan_iter(f'api/v1/locations/all*'):
            r.delete(key)

    def delete(self):
        super().delete()
        r = self.__class__.get_cache_connection()
        for key in r.scan_iter(f'api/v1/locations/all*'):
            r.delete(key)

    @classmethod
    def datatable(cls, vars):
        # START: Common
        columns = vars.get('columns', [])
        columns = {item.get('property'): True for item in columns if item.get('property', '') != ''}
        columns['_id'] = True
        required_filter = vars.pop('required_filter', {})
        limit = int(vars.get('length', 0))
        limit = 0 if limit < 0 else limit
        start = int(vars.get('start', 0))
        query_text = vars.get('search[value]', '')
        sort = [None for i in range(len(columns))]
        for k, v in {k: v for k, v in vars.items() if 'order[' in k and '][column]' in k}.items():
            sort_index = int(k.replace('order[', '').replace('][column]', ''))
            col_index = int(v)
            direction = 1 if vars.get(f'order[{sort_index}][dir]') == 'asc' else -1
            sort[sort_index] = (vars.get(f'columns[{col_index}][data]'), direction)
        sort = [s for s in sort if s != None]
        all = cls.find({}, columns)
        filter = {'$or': [{col.get('property'): {'$regex': query_text, '$options': '-i'}} for col in vars.get('columns', []) if col.get('regex') == True and query_text != '']}
        if filter['$or'] == []:
            filter.pop('$or')
        filter = {**required_filter, **filter}
        _data = cls.find(filter, columns)
        data = list(_data.sort(sort).limit(limit).skip(start))
        # END: Common

        return {'data': data, 'recordsTotal': all.count(), 'recordsFiltered': _data.count()}
