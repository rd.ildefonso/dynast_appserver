import arrow
from settings import DATETIME_FORMAT, DEFAULT_TIMEZONE
from datetime import datetime
from common.mogo import Model, Field, ReferenceField
from models.user import User
from models.formula import Formula


class ManufacturingOrder(Model):
    date_created = Field(datetime, required=True)
    created_by = ReferenceField(User, required=True)

    manufacturing_order_no = Field(str, required=True)
    formula_no = ReferenceField(Formula, required=True)

    batch_size = Field(int, required=True)

    batch_no = Field(str, required=True)
    purchase_order_no = Field(str, required=True)

    # NOTE: 0 - Draft / 1 - Passed to QA / 2 - Approve / -1 - Void
    status = Field(int, required=True, default=0)

    quality_by = ReferenceField(User)
    date_quality = Field(datetime)

    approved_by = ReferenceField(User)
    date_approved = Field(datetime)

    passed_to_manufacturing_by = ReferenceField(User)
    date_passed_to_manufacturing = Field(datetime)

    @classmethod
    def create(cls, **kwargs):
        current_year = arrow.utcnow().datetime.strftime('%y')
        manufacturing_order_no = None
        PADDING = 8
        SUFFIX = 'DYN-MO'
        last_record = cls.find().sort('date_created', -1).limit(1)
        if not last_record:
            manufacturing_order_no = f'{current_year}-00000001-{SUFFIX}'
        else:
            last_record = list(last_record)[0]
            last_no = int(last_record['manufacturing_order_no'].split('-')[1])
            try:
                last_no = str(last_no + 1)
                for z in range(PADDING - len(last_no)):
                    last_no = f'0{last_no}'
                manufacturing_order_no = f'{current_year}-{last_no}-{SUFFIX}'
            except:
                raise Exception('Unable to generate No')
        kwargs['manufacturing_order_no'] = manufacturing_order_no
        if not isinstance(kwargs.get('created_by'), User):
            try:
                kwargs['created_by'] = User.grab(kwargs.get('created_by'))
            except:
                raise Exception('Session has Expired')
        if not isinstance(kwargs['formula_no'], Formula):
            kwargs['formula_no'] = Formula.grab(kwargs['formula_no'])
        kwargs['batch_size'] = int(kwargs['batch_size'])
        return super().create(**kwargs)

    def update(self, **kwargs):
        if kwargs.get('formula_no'):
            if not isinstance(kwargs['formula_no'], Formula):
                kwargs['formula_no'] = Formula.grab(kwargs['formula_no'])
        if kwargs.get('batch_size'):
            kwargs['batch_size'] = int(kwargs['batch_size'])
        super().update(**kwargs)

    @classmethod
    def datatable(cls, vars):
        # START: Common
        columns = vars.get('columns', [])
        columns = {item.get('property'): True for item in columns if item.get('property', '') != ''}
        columns['_id'] = True
        required_filter = vars.pop('required_filter', {})
        limit = int(vars.get('length', 0))
        limit = 0 if limit < 0 else limit
        start = int(vars.get('start', 0))
        query_text = vars.get('search[value]', '')
        sort = [None for i in range(len(columns))]
        for k, v in {k: v for k, v in vars.items() if 'order[' in k and '][column]' in k}.items():
            sort_index = int(k.replace('order[', '').replace('][column]', ''))
            col_index = int(v)
            direction = 1 if vars.get(f'order[{sort_index}][dir]') == 'asc' else -1
            sort[sort_index] = (vars.get(f'columns[{col_index}][data]'), direction)
        sort = [s for s in sort if s != None]
        all = cls.find({}, columns)
        filter = {'$or': [{col.get('property'): {'$regex': query_text, '$options': '-i'}} for col in vars.get('columns', []) if col.get('regex') == True and query_text != '']}
        if filter['$or'] == []:
            filter.pop('$or')
        filter = {**required_filter, **filter}
        _data = cls.find(filter, columns)
        data = list(_data.sort(sort).limit(limit).skip(start))
        # END: Common

        for index, item in enumerate(data):
            data[index] = dict(item)
            if 'formula_no' in item:
                formula = Formula.grab(item['formula_no'].id)
                data[index]['formula_no'] = f'{formula.product_code} - {formula.description} {formula.variant}'
            data[index]['date_created'] = arrow.get(item['date_created']).to(DEFAULT_TIMEZONE).datetime.strftime(DATETIME_FORMAT)
        return {'data': data, 'recordsTotal': all.count(), 'recordsFiltered': _data.count()}

    def change_status(self, target_status, user_id):
        user = User.grab(user_id)
        if target_status == 1:
            formula = Formula.grab(self.formula_no.id)
            for category in ['raw_materials', 'packaging_materials']:
                from models.inventory_transaction import InventoryTransaction
                for item in formula[category]:
                    from models.raw_material import RawMaterial
                    raw_material = RawMaterial.grab(InventoryTransaction._id_type(item['item_code']))
                    if not isinstance(raw_material, RawMaterial):
                        raise Exception('Malformed Records - RM Does not Exist')
                    if category == 'raw_materials':
                        needed_multiplier = float(formula['standard_packing_per_piece']) * float(formula['standard_specific_gravity']) / 1000
                        percentage = float(item['quantity']) / 100
                        factor = needed_multiplier * percentage
                    else:
                        factor = float(item['quantity'])
                    target_quantity = factor * float(self.batch_size)
                    stock_on_hand = InventoryTransaction.get_stock_on_hand(raw_material)
                    if target_quantity > stock_on_hand:
                        raise Exception(f'Records Integrity - Not Enough Stock for {raw_material["description"]}')

                    available_stocks = InventoryTransaction.find({'raw_material.$id': raw_material._id, 'type': 'delivery', 'exhausted': {'$ne': False}}).sort([('date_created', 1)]) # FiFo
                    deducted_quantity = 0
                    used_quality_control_nos = []
                    while deducted_quantity != target_quantity:
                        for stock in available_stocks:
                            used = stock.used_quantity
                            used = 0 if used == None else used
                            remaining = stock.cumulative_quantity - used
                            if remaining >= target_quantity:
                                deducted_quantity = target_quantity
                                used_quality_control_nos.append(stock.quality_control_no)
                                stock.update(used_quantity=used + target_quantity)
                            else:
                                deducted_quantity = deducted_quantity + remaining
                                used_quality_control_nos.append(stock.quality_control_no)
                                stock.update(
                                    used_quantity=stock.cumulative_quantity,
                                    exhausted=True
                                )
                        break
                    InventoryTransaction.create(
                        created_by=user,
                        type='used',
                        raw_material=item['item_code'],
                        cumulative_quantity=float(target_quantity-(2*target_quantity)),
                        manufacturing_order=self,
                        used_quality_control_nos=used_quality_control_nos
                    )
        date_value = arrow.utcnow().datetime
        self.update(
            status=int(target_status),
            passed_to_manufacturing_by=user,
            date_passed_to_manufacturing=date_value)
