import arrow
from settings import DATETIME_FORMAT, DEFAULT_TIMEZONE
from datetime import datetime
from common.mogo import Model, Field, ReferenceField
from models.user import User


class Forecast(Model):
    date_created = Field(datetime, required=True)
    created_by = ReferenceField(User, required=True)
    last_modified_by = ReferenceField(User, required=True)

    forecast_no = Field(str, required=True)
    finished_goods = Field(list, default=[])

    @classmethod
    def create(cls, **kwargs):
        current_year = arrow.utcnow().datetime.strftime('%y')
        forecast_no = None
        PADDING = 8
        SUFFIX = 'DYN-FC'
        last_record = cls.find().sort('created_by', -1).limit(1)
        if not last_record:
            forecast_no = f'{current_year}-00000001-{SUFFIX}'
        else:
            last_record = list(last_record)[0]
            last_no = int(last_record['forecast_no'].split('-')[1])
            try:
                last_no = str(last_no + 1)
                for z in range(PADDING - len(last_no)):
                    last_no = f'0{last_no}'
                forecast_no = f'{current_year}-{last_no}-{SUFFIX}'
            except:
                raise Exception('Unable to generate No')
        if not isinstance(kwargs.get('created_by'), User):
            try:
                kwargs['created_by'] = User.grab(kwargs.get('created_by'))
            except:
                raise Exception('Session has Expired')
        if not isinstance(kwargs.get('last_modified_by'), User):
            try:
                kwargs['last_modified_by'] = User.grab(kwargs.get('last_modified_by'))
            except:
                raise Exception('Session has Expired')
        kwargs['forecast_no'] = forecast_no
        return super().create(**kwargs)

    def update(self, **kwargs):
        if not isinstance(kwargs['last_modified_by'], User):
            kwargs['last_modified_by'] = User.grab(kwargs['last_modified_by'])
        super().update(**kwargs)

    @classmethod
    def datatable(cls, vars):
        # START: Common
        columns = vars.get('columns', [])
        columns = {item.get('property'): True for item in columns if item.get('property', '') != ''}
        columns['_id'] = True
        required_filter = vars.pop('required_filter', {})
        limit = int(vars.get('length', 0))
        limit = 0 if limit < 0 else limit
        start = int(vars.get('start', 0))
        query_text = vars.get('search[value]', '')
        sort = [None for i in range(len(columns))]
        for k, v in {k: v for k, v in vars.items() if 'order[' in k and '][column]' in k}.items():
            sort_index = int(k.replace('order[', '').replace('][column]', ''))
            col_index = int(v)
            direction = 1 if vars.get(f'order[{sort_index}][dir]') == 'asc' else -1
            sort[sort_index] = (vars.get(f'columns[{col_index}][data]'), direction)
        sort = [s for s in sort if s != None]
        all = cls.find({}, columns)
        filter = {'$or': [{col.get('property'): {'$regex': query_text, '$options': '-i'}} for col in vars.get('columns', []) if col.get('regex') == True and query_text != '']}
        if filter['$or'] == []:
            filter.pop('$or')
        filter = {**required_filter, **filter}
        _data = cls.find(filter, columns)
        data = list(_data.sort(sort).limit(limit).skip(start))
        # END: Common

        for index, item in enumerate(data):
            data[index] = dict(item)
            if 'last_modified_by' in item:
                user = User.grab(data[index]['last_modified_by'].id)
                data[index]['last_modified_by'] = user.human_readable() if user else ''
            data[index]['date_last_modified'] = arrow.get(item['date_last_modified']).to(DEFAULT_TIMEZONE).datetime.strftime(DATETIME_FORMAT)
        return {'data': data, 'recordsTotal': all.count(), 'recordsFiltered': _data.count()}
