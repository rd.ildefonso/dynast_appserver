import arrow
from settings import DATETIME_FORMAT, DEFAULT_TIMEZONE
from datetime import datetime
from common.mogo import PolyModel, Field

DEFAULT_LEVELS = ['create', 'read', 'update', 'delete']

DEFAULT_MODULES = [
    'dashboard',
    'clients',
    'materials',
    'delivery_receipts',
    'receiving_reports',
    'forecasts',
    'manufacturing_orders',
    'finished_goods',
    'outgoing_deliveries',
    'formulas',
    'locations'
]

ADMIN_MODULES = [
    'data_setup',
    'users',
    'acls'
]


class ACL(PolyModel):
    date_created = Field(datetime, required=True)
    group = Field(str, default='user')

    name = Field(str, required=True)
    department = Field(str)
    is_viewable = Field(bool, required=True, default=True)
    permissions = Field(dict, required=True)

    @classmethod
    def get_child_key(cls):
        return 'group'

    @classmethod
    def create(cls, **kwargs):
        return super().create(**kwargs)

    @classmethod
    def datatable(cls, vars):
        # START: Common
        columns = vars.get('columns', [])
        columns = {item.get('property'): True for item in columns if item.get('property', '') != ''}
        columns['_id'] = True
        required_filter = vars.pop('required_filter', {})
        limit = int(vars.get('length', 0))
        limit = 0 if limit < 0 else limit
        start = int(vars.get('start', 0))
        query_text = vars.get('search[value]', '')
        sort = [None for i in range(len(columns))]
        for k, v in {k: v for k, v in vars.items() if 'order[' in k and '][column]' in k}.items():
            sort_index = int(k.replace('order[', '').replace('][column]', ''))
            col_index = int(v)
            direction = 1 if vars.get(f'order[{sort_index}][dir]') == 'asc' else -1
            sort[sort_index] = (vars.get(f'columns[{col_index}][data]'), direction)
        sort = [s for s in sort if s != None]
        all = cls.find({}, columns)
        filter = {'$or': [{col.get('property'): {'$regex': query_text, '$options': '-i'}} for col in vars.get('columns', []) if col.get('regex') == True and query_text != '']}
        if filter['$or'] == []:
            filter.pop('$or')
        filter = {**required_filter, **filter}
        _data = cls.find(filter, columns)
        data = list(_data.sort(sort).limit(limit).skip(start))
        # END: Common

        for index, item in enumerate(data):
            data[index] = dict(item)
            data[index]['date_created'] = arrow.get(item['date_created']).to(DEFAULT_TIMEZONE).datetime.strftime(DATETIME_FORMAT)
        return {'data': data, 'recordsTotal': all.count(), 'recordsFiltered': _data.count()}

    def delete(self):
        from models.user import User
        existing_users = User.find({'access_levels$.id': self._id})
        if existing_users.count() > 0:
            raise Exception('Records Integrity - User(s) Exists')
        super().delete()


@ACL.register
class AdminACL(ACL):
    group = Field(str, default='admin')

    permissions = Field(dict, required=True, default=dict({module: {level: True if level == 'read' else False for level in DEFAULT_LEVELS} for module in DEFAULT_MODULES}))


@ACL.register
class SuperAdminACL(ACL):
    group = Field(str, default='super_admin')

    permissions = Field(dict, required=True, default=dict({module: {level: True for level in DEFAULT_LEVELS} for module in (DEFAULT_MODULES + ADMIN_MODULES)}))

    @classmethod
    def check(cls):
        acl = SuperAdminACL.find_one({'name': 'super_admin', 'is_viewable': False})
        permissions = dict({module: {level: True for level in DEFAULT_LEVELS} for module in (DEFAULT_MODULES + ADMIN_MODULES)})
        if acl is None:
            acl = SuperAdminACL.create(
                name='super_admin',
                is_viewable=False,
                permissions=permissions)
        else:
            if acl.permissions != permissions:
                acl.update(permissions=permissions)
        return acl
